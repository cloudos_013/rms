package project.test;

import java.util.List;

import org.durcframework.rms.entity.RUserRole;
import org.durcframework.rms.service.RUserRoleService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class RUserRoleServiceTest extends TestBase {

	@Autowired
	private RUserRoleService userRoleService;
	
	@Test
	public void testGetUserRole(){
		String username = "sellMan";
		List<RUserRole> userRoles = userRoleService.getUserRole(username);
		for (RUserRole rUserRole : userRoles) {
			System.out.println(rUserRole.getRoleId());
		}
	}
}
