package project.test;

import java.util.List;

import org.durcframework.rms.entity.RRole;
import org.durcframework.rms.service.RRoleService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class RRoleServiceTest extends TestBase {

	@Autowired
	private RRoleService roleService;
	
	@Test
	public void testGetUserRole(){
		List<RRole> list = roleService.getUserRole("user_qq");
		for (RRole rRole : list) {
			System.out.println(rRole.getRoleName());
		}
	}
	
}
