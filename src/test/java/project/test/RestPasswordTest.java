package project.test;

import java.util.List;

import org.durcframework.rms.common.Md5Encrypt;
import org.durcframework.rms.entity.RUser;
import org.durcframework.rms.service.RUserService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class RestPasswordTest extends TestBase {

	@Autowired
	RUserService userService;
	
	// 将所有用户密码初始化为123456
	@Test
	public void testList() {
		String newPswd = "123456";
		newPswd = Md5Encrypt.encrypt(newPswd);
		List<RUser> users = userService.getAllUser();
		for (RUser user : users) {
			userService.updateUserPassword(user, newPswd);
		}
	}
	
}
