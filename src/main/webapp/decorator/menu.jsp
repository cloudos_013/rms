<%@page import="org.durcframework.rms.common.UserMenu"%>
<%@page import="org.durcframework.rms.common.RMSContext"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp" %>
<%
request.setAttribute("menus", RMSContext.getInstance().getUserMenu());
%>
<div class="sidebar-wrap">
	<!-- 
	<div class="sidebar-title">
	    <h1>菜单</h1>
	</div>
	 -->
	<div class="sidebar-content">
     	<ul class="sidebar-list">
		<c:forEach items="${menus}" var="menu">
			<li class="rms-menu-cont">
				<a href="#"><i class="icon-font">&#xe003;</i>${menu.text}</a>
				<ul class="sub-menu" style="display: none;">
					<c:forEach items="${menu.children}" var="child">
						<li>
							<a href="${ctx}${child.url}?srId=${child.id}" class="uuid-${child.id}">
							<i class="icon-font">&nbsp;</i>${child.text}
							</a>
						</li>
					</c:forEach>
				</ul>
			</li>
		</c:forEach>
		</ul>
	</div>
</div>
