<%@page import="com.alibaba.fastjson.JSON"%>
<%@page import="org.durcframework.rms.common.RMSContext"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
(function(){
	var dataStr = '<%=JSON.toJSON(RMSContext.getInstance().getCurrentUserPermission()) %>';
	var data = jQuery.parseJSON(dataStr);
	
	var rightData = {};
	for(var key in data) {
		var operateCodes = [];
		var userOperations = data[key];
		
		for(var i=0,len=userOperations.length;i<len;i++) {
			operateCodes.push(userOperations[i].operateCode);
		}
		
		rightData[key] = operateCodes;
	}
	/*结构:
	key/value -> srId/operateCodes
	var permissionData = {
		{"1":["view","update"],"2":["del"]}
		,{"2":["view","del"]}
	}
	*/
	FDRight.setData(rightData);
})();
</script>