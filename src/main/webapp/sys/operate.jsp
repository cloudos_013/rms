<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>操作管理</title>
</head>
<body>
	<div class="search-wrap">
		<div class="search-content">
        	<table class="search-tab">
				<tr>
					<th>操作代码:</th><td id="txtOperateCodeSch"></td>
					<th>操作名称:</th><td id="txtOperateNameSch"></td>
					<td>
						<div id="btnSch"></div>
					</td>
				</tr>
       		</table>
       </div>
   </div>
   <div class="result-wrap">
		<div class="result-title">
             <div class="result-list">
                 <a id="addNew" href="javascript:void(0)"><i class="icon-font"></i>新增操作</a>
             </div>
		</div>
		<div class="result-content">
			<div id="grid"></div>
		</div>
   </div>
   
   <div id="crudWin" style="display: none;">
   		<table class="insert-tab" width="100%">
   			<caption id="formMsg"></caption>
        	<tbody>
                <tr>
                    <th><i class="require-red">*</i>操作代码：</th>
                    <td id="txtOperateCode"></td>
                </tr>
                <tr>
                    <th><i class="require-red">*</i>操作名称：</th>
                    <td id="txtOperateName"></td>
                </tr>
         	</tbody>
         </table>
   </div>
   
	<div id="delWin" style="display: none;">
   		<span class="require-red">该操作正在被使用,无法删除</span>
    	<div id="delGrid"></div>		
	</div>

<script type="text/javascript">
(function(){

var schPanel;
var grid;
var formPanel;
var crudWin;
var delGrid;
var delWin;

var listUrl = ctx + 'listRSysOperate.do';
var addUrl = ctx + 'addRSysOperate.do';
var updateUrl = ctx + 'updateRSysOperate.do';
var delUrl = ctx + 'delRSysOperate.do';

schPanel = new FDFormPanel({
	controls:[
		new FDTextBox({domId:'txtOperateCodeSch',name:'operateCodeSch'})
		,new FDTextBox({domId:'txtOperateNameSch',name:'operateNameSch'})
	]
});

new FDButton({domId:'btnSch',text:'查询',onclick:function(){
	grid.search(schPanel.getData());
}});

grid = new FDGrid({
	domId:'grid'
	,url:listUrl
	,width:'600px'
	,columns:[
  		{text:"操作代码",name:"operateCode"}
  		,{text:"操作名称",name:"operateName"}
  	]
  	,actionButtons:[
  		{text:'修改',onclick:update}
  		,{text:'删除',onclick:del}
  	]
});

crudWin = new FDWindow({
	contentId:'crudWin'
	,title:'添加用户'
	,width:'400px'
	,buttons:[
		{text:'保存',onclick:function(){
			formPanel.save();
		}}
		,{text:'取消',onclick:function(){
			crudWin.hide();
		}}
	]
});

formPanel = new FDFormPanel({
	grid:grid
	,win:crudWin
	// 服务器端的请求
	,crudUrl:{
		add: addUrl
		,update: updateUrl
		,del: delUrl
	}
	,controls:[
		new FDTextBox({domId:'txtOperateCode',name:'operateCode',msgId:'formMsg',width:200
			,validates:[
		     {rule:{notNull:true},successClass:'green',errorClass:'require-red',errorMsg:'操作代码不能为空'}
		     ,{rule:{maxLength:20},successClass:'green',errorClass:'require-red',errorMsg:'操作代码长度不能大于20'}
		     ]
		})
		,new FDTextBox({domId:'txtOperateName',name:'operateName',msgId:'formMsg',width:200
			,validates:[
		     {rule:{notNull:true},successClass:'green',errorClass:'require-red',errorMsg:'操作名称不能为空'}
		     ,{rule:{maxLength:20},successClass:'green',errorClass:'require-red',errorMsg:'操作名称长度不能大于20'}
		     ]
		})
	]
});

$('#addNew').click(function(){
	add();
});

function add() {
	formPanel.getControl('operateCode').enable();
	formPanel.add();
}

function update(rowData,rowIndex) {
	formPanel.update(rowData);
	// 禁用
	formPanel.getControl('operateCode').disable();
}


function del(row,rowIndex) {
	Action.post(ctx + 'listOperateUse.do',row,function(r){
		if(r.operateCodeUsed){
			var title = '删除[<span style="color:red;">'+row.operateCode+'</span>]';
			
			initDelGrid();
			
			delGrid.loadData(r.operateCodeUsedList);
			
			delWin.setTitle(title);
			
			delWin.show();
		}else{
			formPanel.del(row);
		}
	})
}

function initDelGrid() {
	if(!delGrid) {
		delGrid = new FDGrid({
			domId:'delGrid'
			,showPaging:false
			,showSetting:false
			,columns:[
				{name:'operateName',text:'使用地方',width:200,render:function(rowData){
					return rowData.resName + "-" + rowData.operateName;
				}}
			]
		});
		
		delWin = new FDWindow({
			contentId:'delWin'
			,width:'400px'
			,height:'200px'
			,buttons:[
				{text:'关闭',onclick:function() {
					delWin.close();
				}}          
			]
		})
	}
}

})();
</script>
</body>
</html>