<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户管理</title>
<script type="text/javascript" src="${ctx}resources/js/libs/MD5.js"></script>
</head>
<body>
	<div class="search-wrap">
		<div class="search-content">
        	<table class="search-tab">
				<tr>
					<th>用户名:</th><td id="txtUsernameSch"></td>
					<td>
						<div id="btnSch"></div>
					</td>
				</tr>
       		</table>
       </div>
   </div>
   <div class="result-wrap">
		<div class="result-title">
             <div class="result-list">
                 <a id="addNewUser" href="javascript:void(0)"><i class="icon-font"></i>新增用户</a>
             </div>
		</div>
		<div class="result-content">
			<div id="grid"></div>
		</div>
   </div>
   
   <div id="crudWin" style="display: none;">
   		<table class="insert-tab" width="100%">
   			<caption id="addMsg"></caption>
        	<tbody>
                <tr>
                    <th><i class="require-red">*</i>用户名：</th>
                    <td id="txtUsername"></td>
                </tr>
                <tr>
                    <th><i class="require-red">*</i>密码：</th>
                    <td id="txtPassword"></td>
                </tr>
         	</tbody>
         </table>
   </div>

<script type="text/javascript" src="js/SelectRoleWin.js"></script>
<script type="text/javascript">
$(function(){

var grid;
var crudWin;
var formPanel;
var schPanel;

var listUrl = ctx + 'listRUser.do'; // 查询
var addUrl = ctx + 'addRUser.do'; // 添加
var listUserRoleUrl = ctx + 'listUserRRole.do'
var setUserRolesUrl = ctx + 'setUserRoles.do';
var resetUserPasswordUrl = ctx + 'resetUserPassword.do';

schPanel = new FDFormPanel({
	controls:[
		new FDTextBox({domId:'txtUsernameSch',name:'usernameSch'})
	]
});

crudWin = new FDWindow({
	contentId:'crudWin'
	,title:'添加用户'
	,width:'400px'
	,buttons:[
		{text:'保存',onclick:function(){
			saveUser();
		}}
		,{text:'取消',onclick:function(){
			crudWin.hide();
		}}
	]
});

formPanel = new FDFormPanel({
	grid:grid
	,win:crudWin
	// 服务器端的请求
	,crudUrl:{
		add:addUrl
	}
	,onSubmit:function() {
		var b = this.validate();
		if(b) {
			var passwordInput = this.getControl('password');
			var md5 = faultylabs.MD5(passwordInput.getValue());
			passwordInput.setValue(md5);
		}
		return b;
	}
	,controls:[
		new FDTextBox({domId:'txtUsername',name:'username',msgId:'addMsg',width:200
			,validates:[
		     {rule:{notNull:true},successClass:'green',errorClass:'require-red',errorMsg:'用户名不能为空'}
		     ,{rule:{minLength:6,maxLength:16},successClass:'green',errorClass:'require-red',errorMsg:'用户名长度必须为6~16'}
		     ]
		})
		,new FDPasswordBox({domId:'txtPassword',name:'password',msgId:'addMsg',width:200
			,validates:[
		     {rule:{notNull:true},successClass:'green',errorClass:'require-red',errorMsg:'密码不能为空'}
		     ,{rule:{minLength:6,maxLength:16},successClass:'green',errorClass:'require-red',errorMsg:'密码长度必须为6~16'}
		     ]
		})
	]
});

new FDButton({domId:'btnSch',text:'查询',onclick:function(){
	grid.search(schPanel.getValues());
}});

var grid = new FDGrid({
	domId:'grid'
	,url:listUrl
	,columns:[
		{text:"用户名",name:"username"}
		,{text:"用户角色",name:"roles",render:roleFormatter}
		,{text:"添加时间",name:"addTime",sortName:'addTime'}
		,{text:"最后登陆时间",name:"lastLoginDate",sortName:'lastLoginDate'}
	]
	,actionButtons:[
		{text:'重置密码',onclick:resetPassword,operateCode:'resetPassword'}
		,{text:'设置角色',onclick:addRole,operateCode:'setUserRole'}
	]
});

function saveUser() {
	formPanel.submit(function(e){
		if(e.success) {
			grid.refresh();
			crudWin.close();
		}else{
			formPanel.reset();
			Action.execResult(e);
		}
	});
}

function resetPassword(row){
	FDWindow.confirm("确定给"+row.username+"重置密码吗?",function(r) {
		if(r){
			Action.jsonAsyncActByData(resetUserPasswordUrl,row,function(e){
				if(e.success){
					grid.refresh();
					FDWindow.alert('密码重置成功,新密码为:<br><strong style="font-size:14px;color:red;">' + e.message + '</strong>');
				}
			});
		}
	});
}

function addRole(row) {
	var username = row.username;
	Action.post(listUserRoleUrl,{usernameSch:username},function(e){
		var roleIds = [];
		var userRoles = e.rows;
		for(var i=0,len=userRoles.length; i<len; i++) {
			roleIds.push(userRoles[i].roleId);
		}
		SelectRoleWin.show(roleIds,function(groupRoleIds,personRoleIds){
			setUserRole(groupRoleIds,personRoleIds,username);
		});
	});
}

function setUserRole(groupRoleIds,personRoleIds,username) {
	var params = {
		username:username
		,groupRoleIds:groupRoleIds
		,personRoleIds:personRoleIds
	}
	
	Action.post(setUserRolesUrl,params ,function(e){
		Action.execResult(e,function(){
			SelectRoleWin.hide();
			grid.refresh();
		});
	});
}

function roleFormatter(rowData,td,rowIndex){
	var roles = rowData.roles;
	if(!roles || roles.length == 0){
		return '<span style="color:red;">未分配角色</span>';
	}
	
	var roleNameHtml = [];
	for(var i=0,len=roles.length; i<len; i++) {
		roleNameHtml.push(roles[i].roleName);
	}
	
	// 所有的角色名
	var roleNameStr = roleNameHtml.join('、');
	
	var resultStr = ['<div title="'+roleNameStr+'">'];
	
	if(roleNameHtml.length > 6){ 
		for(var i=0; i<6; i++) {
			resultStr.push(roleNameHtml[i] + "、");
		}
		resultStr.push('...');
	}else{
		resultStr.push(roleNameStr);
	}
	
	resultStr.push('</div>');
	
	return resultStr.join('');
}

$('#addNewUser').click(function(){
	formPanel.add();
});

})
</script>
</body>
</html>