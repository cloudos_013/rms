<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>角色管理</title>
</head>
<body>
	<div class="search-wrap">
		<div class="search-content">
        	<table class="search-tab">
				<tr>
					<th>角色名:</th><td id="txtRoleNameSch"></td>
					<th>角色类型:</th><td id="txtRoleTypeSch"></td>
					<td>
						<div id="btnSch"></div>
					</td>
				</tr>
       		</table>
       </div>
   </div>
   <div class="result-wrap">
		<div class="result-title">
             <div class="result-list">
             	<rms:role operateCode="add">
	                 <a id="addNewRole" href="roleForm.jsp?tag=add&roleTypeName=个人角色"><i class="icon-font"></i>新增角色</a>
             	</rms:role>
             </div>
		</div>
		<div class="result-content">
			<div id="grid"></div>
		</div>
   </div>

	<div id="delWin" style="display: none;">
		<span class="require-red">删除后,以下成员将失去该角色及对应的功能.确定删除吗?</span>
    	<div id="delUsernameGrid"></div>		
	</div>

<script type="text/javascript">
(function(){
	
var grid;
var delUsernameGrid;
var schPanel;
var delWin;
var selectedRow;
var delBtn;

var listUrl = ctx + 'listRRole.do'; // 查询
var delUrl = ctx + 'delRRole.do';
var listRoleRelationInfoUrl = ctx + 'listRoleRelationInfo.do';

var ROLE_TYPE = {'1':'个人角色','2':'用户组角色'}
var roleTypeItems = [
	{text:'个人角色',value:1}
	,{text:'用户组角色',value:2}
]

schPanel = new FDFormPanel({
	controls:[
		new FDTextBox({domId:'txtRoleNameSch',name:'roleNameSch'})
		,new FDSelectBox({domId:'txtRoleTypeSch',name:'roleTypeSch',items:roleTypeItems})
	]
});

new FDButton({domId:'btnSch',text:'查询',onclick:function(){
	grid.search(schPanel.getValues());
}});

var grid = new FDGrid({
	domId:'grid'
	,url:listUrl
	,width:'600px'
	,columns:[
		{text:"角色名",name:"roleName",style:{width:'200px'}}
		,{text:"角色类型",name:"roleType",style:{width:'200px'},render:function(data){
			return ROLE_TYPE[data.roleType] || '其它类型';
		}}
	]
	,actionButtons:[
		{text:'修改',onclick:update,operateCode:'update'}
		,{text:'删除',onclick:del,operateCode:'del'}
	]
});

function update(row) {
	var params = [
		'tag=update'
		,'roleId=' + row.roleId
		,'roleTypeName=' + encodeURIComponent(row.roleTypeName)
		,'roleName=' + encodeURIComponent(row.roleName)
	];
	window.location = 'roleForm.jsp?' + params.join('&');
}

function del(row) {
	if (row){
		selectedRow = row;
		var userRoles = listRoleUser(row);

		if(userRoles.length > 0){
			initDelGrid();
			
			delUsernameGrid.loadData(userRoles);
			
			initDelWin();
			
			var title = '删除[<span style="color:red;">'+row.roleName+'</span>]角色';
			delWin.setTitle(title);
			
			delBtn.setOnclick(function(){
				doDel(row);
			});
			
			delWin.show();
		}else{
			FDWindow.confirm('确定删除该角色吗?',function(r){
				if(r){
					doDel(row);
				}
			});
		}
	}
}

function doDel(delRow){
	if(delRow){
		Action.post(delUrl,delRow,function(result){
			Action.execResult(result,function(){
				FDWindow.alert('删除成功',function(){
					if(delWin){
						delWin.close();
					}
					grid.refresh();
				});
			});
		});
	}
}


function initDelWin() {
	if(!delWin) {
		delBtn = new FDButton({text:'删除'});
		
		delWin = new FDWindow({
			contentId:'delWin'
			,width:'400px'
			,height:'200px'
			,buttons:[
		  		delBtn
		  		,{text:'取消',onclick:function(){
		  			delWin.hide();
		  		}}
		  	]
		})
	}
}

function initDelGrid() {
	if(!delUsernameGrid){
		delUsernameGrid = new FDGrid({
			domId:'delUsernameGrid'
			,showPaging:false
			,showSetting:false
			,columns:[
				{text:"用户名",name:"username",style:{width:'200px'}}
			]
		});
	}
}

function listRoleUser(row){
	var ret = [];
	
	Action.postSync(listRoleRelationInfoUrl
			,{roleId:row.roleId},function(result){
		if(result.success){
			ret = result.userRoles;
		}
	});
	
	return ret;
}

})()
</script>
</body>
</html>