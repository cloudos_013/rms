<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户组管理</title>
<script type="text/javascript" src="${resources}js/libs/FunUtil.js"></script>
<style type="text/css">
.left-btns {padding: 5px;}
#group-right{padding: 10px;}
</style>
</head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户组管理</title>
</head>
<body>
<div class="sidebar-wrap" style="width: 260px;background: #fff;">
        <div class="sidebar-content">
        	<div class="left-btns">
        	<rms:role operateCode="optRoot">
	        	<a id="btnAddRoot" href="javascript:void(0)"><i class="icon-font"></i>新增用户组</a>
        	</rms:role>
        	</div>
        	<div id="groupTree"></div>
        </div>
    </div>
    <div style="margin-left: 260px;">
    	<div id="rightTip" style="padding: 20px;"><h3>☜点击用户组查看信息</h3></div>
		<div id="rightCont" style="display: none;">
				<div id="group-right">
				 <fieldset>
			        <legend>基础信息</legend>
					<div class="result-content">
						<div id="hid-groupId"></div>
						<div id="hid-parentId"></div>
						<table class="search-tab" width="100%">
			        		<tbody>
				                <tr>
				                    <th width="100"></th>
				                    <td id="formMsg_" style="line-height:20px;"></td>
				                </tr>
			                </tbody>
			            </table>
			            <table class="insert-tab" width="100%">
			                <tr>
					            <th width="100"><i class="require-red">*</i>用户组名称:</th><td id="txt-groupName"></td>
					        </tr>
					        <tr>
					        	<td></td>
					        	<td id="btnSave"></td>
					        </tr>
			            </table>
		            </div>
				</fieldset>
				<br>
				<div id="tab"></div>
				
				<div id="tab1-cont" style="display: none;">
					<div class="search-wrap-nopadding">
			          	<div class="search-content">
				        	<table class="search-tab">
								<tr>
									<th>用户名:</th><td id="txt-usernameSch"></td>
									<td>
										<div id="btnUserSch"></div>
									</td>
								</tr>
				       		</table>
				       </div>
			       </div>
			       <div class="result-wrap-nopadding">
				       <div class="result-title">
				             <div class="result-list">
				             <rms:role operateCode="addGroupUser">
				                 <a id="addUser" href="javascript:void(0)"><i class="icon-font"></i>添加成员</a>
				             </rms:role>
				             </div>
						</div>
						<div class="result-content">
							<div id="gridUser"></div>
						</div>
					</div>
				</div>
				<div id="tab2-cont" style="display: none;">
					<div class="search-content">
					<rms:role operateCode="addRole">
				        	<table class="search-tab">
				        		<caption id="addRoleMsg"></caption>
								<tr>
									<th>角色名称:</th><td id="txt-roleName"></td>
									<td>
										<div id="btnRoleAdd"></div>
									</td>
								</tr>
				       		</table>
				    </rms:role>
				    </div>
					<div class="result-content">
						<div id="gridRole"></div>
					</div>
				</div>
			</div>
		</div>
    </div>
    
    <div id="addNodeWin" style="display: none;">
    	<div id="hidParentId"></div>
		<table class="insert-tab" width="100%">
			<caption id="formMsg"></caption>
			<tr>
            	<th width="100"><i class="require-red">*</i>用户组名称:</th><td id="txtGroupName"></td>
        	</tr>
          </table>
    </div>
    
    <jsp:include page="groupUser.jsp"></jsp:include>

<script type="text/javascript" src="js/groupRole.js"></script>
<script type="text/javascript" src="js/groupUser.js"></script>
<script type="text/javascript" src="js/group.js"></script>
</body>
</html>