<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div id="addUserWin" style="display: none;">
	<div class="search-wrap">
		<div class="search-content">
	       	<table class="search-tab">
				<tr>
					<th>用户名:</th><td id="txt-usernameNoAddSch"></td>
					<td>
						<div id="btnAddUserSch"></div>
					</td>
				</tr>
      		</table>
		</div>
	</div>
	<div class="result-wrap">
		<div class="result-content">
			<div>请选择待添加的用户,已选择<strong id="addUserCnt">0</strong>位用户</div>
			<div id="gridAddUser"></div>
		</div>
	</div>
</div>
