<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<style type="text/css">
#sysResTree label{margin-right: 5px;}
.checkbox-on{background-color: #ccc;font-weight: bold;}
</style>
</head>
<body>
   <div class="result-wrap">
		<div class="result-content">
			<div id="hidRoleId"></div>
			<table class="search-tab" width="100%">
        		<tbody>
	                <tr>
	                    <th width="120"></th>
	                    <td id="formMsg" style="line-height:20px;"></td>
	                </tr>
                </tbody>
            </table>
            
			<table class="insert-tab" width="100%">
        	<tbody>
                <tr>
                    <th width="120"><i class="require-red">*</i>角色名：</th>
                    <td id="txtRoleName" width="200"></td>
                    <td>角色类型：${param.roleTypeName}</td>
                </tr>
                <tr>
                    <th><i class="require-red">*</i>权限：</th>
                    <td colspan="2">
                    	<div style="line-height:20px;">
                    	<a id="treeSwitch" href="javascript:void(0)">(-)全部折叠</a>
                    	</div>
						<div id="sysResTree" style="line-height:15px;"></div>
                    </td>
                </tr>
                <tr>
                     <th></th>
                     <td colspan="2">
                         <input id="saveBtn" class="btn btn-primary btn6 mr10" value="保存" type="button">
                         <input class="btn btn6" onclick="history.go(-1)" value="返回" type="button">
                     </td>
                 </tr>
         	</tbody>
         </table>
		</div>
   </div>

<script type="text/javascript">
var tag = '${param.tag}';
var isAdd = tag == 'add';
var isUpdate = tag == 'update';
var schRoleId = '${param.roleId}';
var schRoleName = decodeURIComponent('${param.roleName}');
var submitUrl;

if(isAdd) {
	submitUrl = ctx + 'addRRole.do'; // 添加
	document.title = '新增角色';
}else if(isUpdate) {
	document.title = '修改角色';
	submitUrl = ctx + 'updateRRole.do';
}else{
	alert('无效页面');
	history.go(-1);
}
</script>

<script type="text/javascript">
(function(){
var tree;
var formPanel;
var $formMsg = $('#formMsg');
var $sysResTree = $('#sysResTree');

var listRoleMenu = ctx + 'listRoleMenu.do';
var listRolePermissionByRoleIdUrl = ctx + 'listRolePermissionByRoleId.do';


formPanel = new FDFormPanel({
	controls:[
		new FDTextBox({domId:'txtRoleName',name:'roleName',msgId:'formMsg',width:200
			,validates:[
		     {rule:{notNull:true},successClass:'green',errorClass:'require-red',errorMsg:'角色名不能为空'}
		     ,{rule:{maxLength:20},successClass:'green',errorClass:'require-red',errorMsg:'角色名长度不能超过20'}
		     ]
		})
		,new FDHidden({domId:'hidRoleId',name:'roleId',defaultValue:0})
	]
});
	
var tree = new FDTree({
	domId:'sysResTree'
	,url:listRoleMenu
	,clickToggle:true
	,valueFieldName:'srId'
	,textFieldName:'resName'
	,childrenFieldName:'children'
	,render:function(node){
		var text = node.resName;
		if(node.sysFuns && node.sysFuns.length > 0){
			text +='&nbsp;|&nbsp;' + buildOperateCheckbox(node.sysFuns)
		}
		return text;
	}
	,highlightHandler:function(node){
		return false;
	}
});

if(isUpdate){
	
	var data = {
		roleId:schRoleId
		,roleName:schRoleName
	}
	
	Action.post(listRolePermissionByRoleIdUrl,{roleId:schRoleId},function(sysFuns){
		var sfIds = [];
		for(var i=0,len=sysFuns.length;i<len;i++){
			sfIds.push(sysFuns[i].sfId);	
		}

		formPanel.setValues(data);
		
		$sysResTree.find(':checkbox').val(sfIds);
		
		addEffect();
	});
}


function buildOperateCheckbox(sysFuns){
	var html = [];
	var sysFun = null;
	for(var i=0,len=sysFuns.length;i<len;i++){
		sysFun = sysFuns[i];
		html.push('<label>')
		html.push('<input name="sfId" type="checkbox" value="'+sysFun.sfId+'"/>'+sysFun.operateName);
		html.push('</label>');
	}
	
	return html.join('');
}


$('#saveBtn').click(function(){
	if(check()){
		doSave();
	}
});

function doSave() {
	var $inputs = $sysResTree.find('input:checked');
	var sfId = [];
	var params = formPanel.getData();
	$inputs.each(function(){
		sfId.push(this.value);
	});
	params.sfId = sfId;
	
	Action.post(submitUrl,params,function(e){
		Action.execResult(e,function(){
			FDWindow.alert('保存成功');
		})
	})
}

function check() {
	return formPanel.validate();
}

function checkSelect() {
	var $inputs = $sysResTree.find('input:checked');
	var ret = $inputs.length > 0;
	if(!ret){
		$formMsg.addClass('require-red').html('请勾选权限');
	}
	return ret;
}

var state = 1;

function expAll(target) {
	target.innerHTML = ('(-)全部折叠');
	tree.expandAll();
	state = 1;
}

function colAll(target) {
	target.innerHTML = ('(+)全部展开');
	tree.collapseAll();
	state = 0;
}

$("#treeSwitch").click(function(){
	if(!state) {
		expAll(this);
	}else{
		colAll(this);
	}
});

tree.expandAll();

NavUtil.make('角色管理');

$sysResTree.find(':checkbox').click(function(){
	if(this.checked) {
		$(this).parent().addClass('checkbox-on');
	}else{
		$(this).parent().removeClass('checkbox-on');
	}
});

function addEffect(){
	$sysResTree.find(':checkbox').each(function(){
		if(this.checked) {
			$(this).parent().addClass('checkbox-on');
		}else{
			$(this).parent().removeClass('checkbox-on');
		}
	});
}
	
})()
</script>
</body>
</html>