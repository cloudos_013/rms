<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>资源管理</title>
<script type="text/javascript" src="${resources}js/libs/FunUtil.js"></script>
<style type="text/css">
.left-btns {padding: 5px;}
</style>
</head>
<body>
    <div class="sidebar-wrap" style="width: 260px;background: #fff;">
        <div class="sidebar-content">
        	<div class="left-btns">
        		<rms:role operateCode="optRoot">
		        	<a id="btnAddRoot" href="javascript:void(0)"><i class="icon-font"></i>添加根节点</a>
        		</rms:role>
        	</div>
        	<div id="sysResTree"></div>
        </div>
    </div>
    <div style="margin-left: 260px;">
    	<div id="rightTip" style="padding: 20px;"><h3>☜点击菜单查看信息</h3></div>
		<div id="rightCont" style="display: none;">
			<div id="tabNode"></div>
					<!-- tab内容 -->
				<div id="tab1-cont1">
				 <fieldset>
			        <legend>基础信息</legend>
					<div class="result-content">
						<div id="hidSrId_"></div>
						<div id="hidParentId_"></div>
						<table class="search-tab" width="100%">
			        		<tbody>
				                <tr>
				                    <th width="100"></th>
				                    <td id="formMsg_" style="line-height:20px;"></td>
				                </tr>
			                </tbody>
			            </table>
			            <table class="insert-tab" width="100%">
			                <tr>
					            <th width="100"><i class="require-red">*</i>资源名称:</th><td id="txtResName_"></td>
					        </tr>
			                <tr>
					            <th><i class="require-red">*</i>URL:</th><td id="txtUrl_"></td>
					        </tr>
					        <tr>
					        	<td></td>
					        	<td id="btnSave"></td>
					        </tr>
			            </table>
		            </div>
				</fieldset>
				<br>
				<fieldset>
		          	<legend>操作权限</legend>
		          	<div class="search-content">
		          	<rms:role operateCode="addOpt">
			        	<table class="insert-tab" width="100%">
			        		<caption id="addOptMsg"></caption>
							<tr><th width="100"><i class="require-red">*</i>权限代码:</th><td id="txt-operateCode"></td></tr>
							<tr><th><i class="require-red">*</i>权限名称:</th><td id="txt-operateName"></td></tr>
							<tr><th>URL:</th><td id="txt-url"></td></tr>
							<tr><td></td><td> <div id="btnOptAdd"></div></td></tr>
			       		</table>
			       	</rms:role>
			       </div>
					<div class="result-content" style="padding-top: 10px;">
						<div id="gridOpt"></div>
					</div>
				</fieldset>
			</div>
		</div>
    </div>
    
    <div id="addNodeWin" style="display: none;">
    	<div id="hidParentId"></div>
		<table class="insert-tab" width="100%">
			<caption id="formMsg"></caption>
			<tr>
            	<th width="100"><i class="require-red">*</i>资源名称:</th><td id="txtResName"></td>
        	</tr>
           	<tr>
            	<th>URL:</th><td id="txtUrl"></td>
        	</tr>
          </table>
    </div>

<script type="text/javascript" src="js/SelectRoleWin.js"></script>
<script type="text/javascript" src="js/sysRes.auth.js"></script>
<script type="text/javascript" src="js/sysRes.js"></script>
</body>
</html>