/**
 * 角色选择窗口
 * 使用方法:SelectRoleWin.show(roleIds,callback);
 */
var SelectRoleWin = (function(){
	
	var listTreeRoleUrl = ctx + 'listTreeRole_backuser.do';
	var listPersonRoleUrl = ctx + 'listAllRRole_backuser.do?roleTypeSch=1';
	
	var inited = false;
	var tab = null;
	var win = null;
	var schPanel = null
	var tree = null;
	var okBtn = null;
	
	var html = [
		'<div id="roleSelectPanel">'
			/*
			,'<div>'
				,'<div class="search-content">'
					,'<table class="search-tab">'
						,'<tr>'
							,'<th>角色名:</th><td id="roleNameSch"></td>'
							,'<td>'
							,'<div id="btnRoleSch"></div>'
							,'</td>'
						,'</tr>'
					,'</table>'
				,'</div>'
			,'</div>'
			*/
			,'<div>'
				,'<div class="result-content">'
					,'<div id="srw_tab"></div>'
				,'</div>'
			,'</div>'
			,'<div>'
				,'<div id="roleIdCont" class="result-content">'
					,'<div id="srw_tab1-cont"><div id="srw_treeRole"></div></div>'
					,'<div id="srw_tab2-cont"><div id="srw_personRole"></div></div>'
				,'</div>'
			,'</div>'
		,'</div>'
	];
					
	function init(callback) {
		if(!inited) {
			inited = true;
			
			initPanel();
		}
		
		okBtn.setOnclick(function(){
			ok(callback);
		});
		
		//reset();
		
		srw_tab.selectItemByValue(1);
		
		win.show();
	}
	
	function ok(callback) {
		var groupRoleIds = [];
		var personRoleIds = [];
		$('#srw_treeRole').find('input[name=roleId]:checked').each(function(){
			groupRoleIds.push(this.value);
		});
		$('#srw_personRole').find('input[name=roleId]:checked').each(function(){
			personRoleIds.push(this.value);
		});
		callback(groupRoleIds,personRoleIds);
	}
	
	function initPanel() {
		var panel = html.join('');
		$('body').append(panel);
		
		srw_tab = new FDTab({
			domId:'srw_tab'
			,items:[
				{text:'用户组角色',value:1,contentId:'srw_tab1-cont'}
				,{text:'个人角色',value:2,contentId:'srw_tab2-cont'}
			]
		});
		
		okBtn = new FDButton({text:'确定'})
		
		win = new FDWindow({
			contentId:'roleSelectPanel'
			,title:'选择角色'
			,width:'600px'
			,height:'420px'
			,modal:true  
			,buttons:[
				okBtn
				,new FDButton({text:'取消',onclick:function(){
					win.close();
				}})
			]
		});
		
		/*
		schPanel = new FDFormPanel({
			controls:[
				new FDTextBox({domId:'roleNameSch',name:'roleNameSch'})
			]
		});
		
		new FDButton({domId:'btnRoleSch',text:'查询',onclick:function(){
			search();
		}});
		* 
		*/
		
		tree = new FDTree({
			domId:'srw_treeRole'
			,url:listTreeRoleUrl
			,valueFieldName:'groupId'
			,textFieldName:'groupName'
			,childrenFieldName:'children'
			,clickToggle:true
			,showBorder:false
			,render:function(node){
				var text = node.groupName;
				if(node.roles && node.roles.length > 0){
					text +='&nbsp;:&nbsp;' 
						+ buildCheckbox(node.roles);
				}
				return text;
			}
			,highlightHandler:function(node){
				return false;
			}
		});
		
		tree.expandAll();
		
		buildPersonRole();
		
		addCheckboxEffect('roleSelectPanel');
	}
	
	function addCheckboxEffect(parentDomId) {
		$('#' + parentDomId).delegate('input','click',function(){
			doAffect(this);
		});
	}
	
	
	function buildPersonRole() {
		Action.postSync(listPersonRoleUrl,{},function(data){
			var roles = data.rows;
			var html = [];
			var role = null;
			for(var i=0,len=roles.length;i<len;i++){
				role = roles[i];
				if(i > 0) {
					html.push('<br>');
				}
				html.push('<label>')
				html.push('<input name="roleId" type="checkbox" value="'+role.roleId+'" />'+role.roleName);
				html.push('</label>');
			}
			
			var checkboxHtml = html.join('');
			$('#srw_personRole').html(checkboxHtml);
		})
	}
	
	function buildCheckbox(roles){
		var html = [];
		var role = null;
		for(var i=0,len=roles.length;i<len;i++){
			role = roles[i];
			html.push('<label style="margin-right: 5px;">')
			html.push('<input name="roleId" type="checkbox" value="'+role.roleId+'" />'+role.roleName);
			html.push('</label>');
		}
		
		return html.join('');
	}
	
	function search() {
		tree.reload(schPanel.getData());
	}
	
	function reset() {
		schPanel.reset();
	}
	
	function setValue(roleIds) {
		$('#roleSelectPanel')
			.find('input[name=roleId]')
			.attr('checked',false)
			.val(roleIds);
			
		$('#roleSelectPanel').find('input[name=roleId]').each(function(){
			doAffect(this);
		});
	}
	
	function doAffect(checkbox) {
		if(checkbox.checked) {
			$(checkbox).parent().css({'font-weight':'bold','background-color':'#ccc'})
		}else{
			$(checkbox).parent().css({'font-weight':'normal','background-color':''})
		}
}
	
	return {
		show:function(roleIds,callback) {
			init(callback);
			setValue(roleIds);
		}
		,hide:function() {
			win.hide();
		}
	};
	
	
})();
