var addRoleForm;
var gridRole;
var addRoleUrl = ctx + 'addRGroupRole.do';
var delRoleUrl = ctx + 'delRGroupRole.do';
var listRoleUrl = ctx + 'listRoleByGroupId.do';

function initGroupRole() {
	if(!addRoleForm) {
		addRoleForm = new FDFormPanel({
			controls:[
				new FDTextBox({domId:'txt-roleName',name:'roleName',msgId:'addRoleMsg',validates:[
					{rule:{notNull:true},successClass:'green',errorClass:'require-red',errorMsg:'角色名称不能为空'}
					,{rule:{maxLength:20},successClass:'green',errorClass:'require-red',errorMsg:'角色名称长度不能超过20'}
				]})
			]
		});
		
		FDRight.checkByCode('addRole',function(){
			new FDButton({domId:'btnRoleAdd',text:'添加角色',onclick:function(){
				addRole();
			}})
		});
		
		
		gridRole = new FDGrid({
			domId:'gridRole'
				,width:'600px'
				,id:'roleId'
				,url:listRoleUrl
				,loadSearch:false
				,showPaging:false
				,showSetting:false
				,columns:[
			  		{text:"角色名称",name:"roleName"}
			  	]
				,actionButtons:[
					{text:'删除',operateCode:'delRole',onclick:function(rowData){
						var msg = '确定要删除<strong>'+rowData.roleName+'</strong>吗?';
						FDWindow.confirm(msg,function(r){
							if(r){
								doDelRole(rowData);
							}
						});
					}}
				]
		});
	}
}

function searchRole() {
	var data = {}
	data.groupId = updateNodeForm.getData('groupId');
	gridRole.search(data);
}

function addRole() {
	if(addRoleForm.validate()) {
		var data = addRoleForm.getData();
		data.groupId = updateNodeForm.getData('groupId');
		doAddRole(data);
	}
}

function doAddRole(row){
	Action.post(addRoleUrl,row,function(data){
		Action.execResult(data,function(){
			searchRole();
			addRoleForm.reset();
		});
	})
}

function doDelRole(row){
	Action.post(delRoleUrl,row,function(result){
		Action.execResult(result,function(result){
			searchRole();
		});
	});
}