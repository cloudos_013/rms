var listRolePermissionSfIdUrl = ctx + 'listRolePermissionSfId_backuser.do';
var setSysFunctionRoleUrl = ctx + 'setSysFunctionRole.do';
// 资源管理授权功能
function auth(rowData) {
	var sfId = rowData.sfId;
	Action.post(listRolePermissionSfIdUrl,{sfId:sfId},function(rolePerms){
		var roleIds = [];
		for(var i=0,len=rolePerms.length; i<len; i++) {
			roleIds.push(rolePerms[i].roleId);
		}
		SelectRoleWin.show(roleIds,function(groupRoleIds,personRoleIds){
			okHandler(groupRoleIds,personRoleIds,sfId);
		});
	});
}

function okHandler(groupRoleIds,personRoleIds,sfId) {
	var roleIds = groupRoleIds.concat(personRoleIds);
	var data = {sfId:sfId,roleId:roleIds};
	// 角色授权
	Action.post(setSysFunctionRoleUrl,data,function(e){
		Action.execResult(e,function(){
			SelectRoleWin.hide();
		})
	});
}
