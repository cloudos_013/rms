// 资源管理
(function(){

var sysResTree;
var addNodeWin;
var addNodeForm;
var updateNodeForm;
var addOptForm;
var gridOpt;
var listAllMenuUrl = ctx + 'listAllMenu.do';
var addUrl = ctx + 'addRSysRes.do';
var updateUrl = ctx + 'updateRSysRes.do';
var delResUrl = ctx + 'delRSysRes.do';

var addOptUrl = ctx + 'addSysFunction.do';
var delOptUrl = ctx + 'delRSysFunction.do';
var listOptUrl = ctx + 'listSysFunctionBySrId.do';
var $rightTip = $('#rightTip');
var $rightCont = $('#rightCont');

addNodeForm = new FDFormPanel({
	controls:[
		new FDHidden({domId:'hidParentId',name:'parentId',defaultValue:0})
		,new FDTextBox({domId:'txtResName',name:'resName',msgId:'formMsg'
			,validates:[
		     	{rule:{notNull:true},successClass:'green',errorClass:'require-red',errorMsg:'资源名称不能为空'}
		     	,{rule:{maxLength:20},successClass:'green',errorClass:'require-red',errorMsg:'资源名称长度不能超过20'}
		     ]
		})
		,new FDTextBox({domId:'txtUrl',name:'url',msgId:'formMsg',width:'300px'})
	]
});

var regEn = /^[a-zA-Z]{1,20}$/;
FDValidateStore.wholeEN = function(val){
	return regEn.test(val);
}

addOptForm = new FDFormPanel({
	controls:[
		new FDTextBox({domId:'txt-operateCode',name:'operateCode',msgId:'addOptMsg',validates:[
			{rule:{notNull:true},successClass:'green',errorClass:'require-red',errorMsg:'权限代码不能为空'}
			,{rule:{maxLength:20},successClass:'green',errorClass:'require-red',errorMsg:'权限代码长度不能超过20'}
			,{rule:{wholeEN:true},successClass:'green',errorClass:'require-red',errorMsg:'权限代码必须为全英文字母'}
		]})
		,new FDTextBox({domId:'txt-operateName',name:'operateName',msgId:'addOptMsg',validates:[
			{rule:{notNull:true},successClass:'green',errorClass:'require-red',errorMsg:'权限名称不能为空'}
			,{rule:{maxLength:20},successClass:'green',errorClass:'require-red',errorMsg:'权限名称长度不能超过20'}
		]})
		,new FDTextBox({domId:'txt-url',name:'url',msgId:'addOptMsg',width:300,validates:[
			,{rule:{maxLength:200},successClass:'green',errorClass:'require-red',errorMsg:'URL长度不能超过200'}
		]})
	]
});

updateNodeForm = new FDFormPanel({
	controls:[
	  		new FDHidden({domId:'hidSrId_',name:'srId',msgId:'formMsg_'
	  			,validates:[
	  		     	{rule:{notNull:true},successClass:'green',errorClass:'require-red',errorMsg:'请选择节点'}
	  		     ]
	  		})
	  		,new FDHidden({domId:'hidParentId_',name:'parentId',defaultValue:0})
	  		,new FDTextBox({domId:'txtResName_',name:'resName',msgId:'formMsg_'
	  			,validates:[
	  		     	{rule:{notNull:true},successClass:'green',errorClass:'require-red',errorMsg:'资源名称不能为空'}
	  		     	,{rule:{maxLength:20},successClass:'green',errorClass:'require-red',errorMsg:'资源名称长度不能超过20'}
	  		     ]
	  		})
	  		,new FDTextBox({domId:'txtUrl_',name:'url',msgId:'formMsg_',width:'300px'
	  			,validates:[
	  		     	{rule:{notNull:true},successClass:'green',errorClass:'require-red',errorMsg:'URL不能为空'}
	  		     	,{rule:{maxLength:100},successClass:'green',errorClass:'require-red',errorMsg:'URL长度不能超过100'}
	  		     ]
	  		})
	  	]
	  });

FDRight.checkByCode('optRoot',function(){
	$('#btnAddRoot').click(function(){
		addRootMenu();
	});
});

new FDButton({domId:'btnSave',text:'保存',operateCode:'save',onclick:function(){
	saveRes();
}})
new FDButton({domId:'btnOptAdd',text:'添加权限',operateCode:'addOpt',onclick:function(){
	addOpt();
}})

sysResTree = new FDTree({
	domId:'sysResTree'
	,url:listAllMenuUrl
	,valueFieldName:'srId'
	,textFieldName:'resName'
	,childrenFieldName:'children'
	,onclick:function(node) {
		bindData(node);
	}
	,render:function(node){
		var text = node.text ;
		text += '&nbsp;' + buildTreeButton(node);
		return text;
	}
});

sysResTree.expandAll();

gridOpt = new FDGrid({
	domId:'gridOpt'
		,id:'operateCode'
		,showPaging:false
		,showSetting:false
		,columns:[
	  		{text:"权限代码",name:"operateCode"}
	  		,{text:"权限名称",name:"operateName"}
	  		,{text:"URL",name:"url"}
	  	]
		,actionButtons:[
			{text:'授权',operateCode:'addOpt'
			,onclick:function(rowData){
				auth(rowData);
			}}
			,{text:'删除',operateCode:'delOpt'
			,onclick:function(rowData){
				var msg = '确定要删除<strong>'+rowData.operateName+'</strong>吗?';
				FDWindow.confirm(msg,function(r){
					if(r){
						doDelOpt(rowData);
					}
				});
			}}
		]
});

function onloadHandler(rowData,selector,rowIndex,tr){
	if(rowData.sfId) {
		gridOpt.checkRow(rowIndex);
	}
}

new FDTab({
	domId:'tabNode'
	,items:[
		{text:'资源信息',value:1,contentId:'tab1-cont1'}
	]
});

function buildTreeButton(node){
	var html = [];
	//FDRight.checkByCode('optRoot',function(){
		html.push('<a onclick="'+FunUtil.createFun(window,'addChildNode',node)+' return false;">[添加子节点]</a>');
		if(node.children.length == 0) {
			html.push('&nbsp;<a onclick="'+FunUtil.createFun(window,'delSysRes',node)+' return false;">[删除节点]</a>');
		}
	//});
	return html.join('');
}

function addRootMenu(){
	initAddNodeWin();
	addNodeWin.setTitle('添加根节点');
	addNodeForm.reset();
	addNodeWin.show();
}

window.addChildNode = function(node){
	initAddNodeWin();
	addNodeWin.setTitle('添加['+node.resName+']子节点');
	addNodeForm.reset();
	
	addNodeForm.getControl('parentId').setValue(node.srId);
	
	addNodeWin.show();
}

function initAddNodeWin() {
	if(!addNodeWin){
		addNodeWin = new FDWindow({
			contentId:'addNodeWin'
			,width:'500px'
			,buttons:[
				{text:'保存',onclick:function(){
					saveNode();
				}}
				,{text:'取消',onclick:function(){
					addNodeWin.close();
				}}
			]
		});
	}
}

function saveNode() {
	var url = addUrl;
	
	if(addNodeForm.validate()) {
		Action.post(url,addNodeForm.getData(),function(result){
			Action.execResult(result,function(result){
				addNodeWin.close();
				reloadTree();
			});
		});
	}
}

function reloadTree() {
	sysResTree.reload();
	sysResTree.expandAll();
}

window.delSysRes = function(node){
	var self = this;
	if (node) {
		var msg = '确定要删除<strong>'+node.resName+'</strong>吗?';
		FDWindow.confirm(msg,function(r){
			if (r){
				Action.post(delResUrl,{srId:node.srId,resName:node.resName},function(result){
					Action.execResult(result,function(result){
						reloadTree();
						$rightTip.show();
						$rightCont.hide();
						FDWindow.alert('删除成功');
					});
				});
			}
		});
	}
}

function bindData(node) {
	updateNodeForm.setData(node);
	gridOpt.setUrl(listOptUrl + '?srId=' + node.srId);
	
	searchOpt();
	
	$rightTip.hide();
	$rightCont.show();
}

function searchOpt() {
	var data = {}
	data.srIdSch = updateNodeForm.getData('srId');
	gridOpt.search(data);
}

function addOpt() {
	if(addOptForm.validate()) {
		var data = addOptForm.getData();
		data.srId = updateNodeForm.getData('srId');
		doAddOpt(data);
	}
}

function saveRes(){
	if(updateNodeForm.validate()) {
		var data = updateNodeForm.getData();
		Action.post(updateUrl,data,function(result){
			Action.execResult(result,function(result){
				reloadTree();
				FDWindow.alert('修改成功');
			});
		});
	}	
}

function doAddOpt(row){
	Action.post(addOptUrl,row,function(data){
		Action.execResult(data,function(){
			searchOpt();
			addOptForm.reset();
		});
	})
}

function doDelOpt(row){
	Action.post(delOptUrl,row,function(result){
		Action.execResult(result,function(result){
			searchOpt();
		});
	});
}

})()
