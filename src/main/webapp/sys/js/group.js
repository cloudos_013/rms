var groupTree;
var addNodeWin;
var addNodeForm;
var updateNodeForm;

var listAllGroupUrl = ctx + 'listAllGroup.do';
var addGroupUrl = ctx + 'addRGroup.do';
var updateGroupUrl = ctx + 'updateRGroup.do';
var delRGroupUrl = ctx + 'delRGroup.do';

var tab;
var $rightTip = $('#rightTip');
var $rightCont = $('#rightCont');

tab = new FDTab({
	domId:'tab'
	,items:[
		{text:'用户组成员',operateCode:'viewGroupUser',value:1,contentId:'tab1-cont'}
		,{text:'用户组角色',operateCode:'viewGroupRole',value:2,contentId:'tab2-cont'}
	]
});

addNodeForm = new FDFormPanel({
	controls:[
		new FDHidden({domId:'hidParentId',name:'parentId',defaultValue:0})
		,new FDTextBox({domId:'txtGroupName',name:'groupName',msgId:'formMsg'
			,validates:[
		     	{rule:{notNull:true},successClass:'green',errorClass:'require-red',errorMsg:'用户组名称不能为空'}
		     	,{rule:{maxLength:20},successClass:'green',errorClass:'require-red',errorMsg:'用户组名称长度不能超过20'}
		     ]
		})
	]
});


updateNodeForm = new FDFormPanel({
	controls:[
	  		new FDHidden({domId:'hid-groupId',name:'groupId',msgId:'formMsg_'
	  			,validates:[
	  		     	{rule:{notNull:true},successClass:'green',errorClass:'require-red',errorMsg:'请选择节点'}
	  		     ]
	  		})
	  		,new FDHidden({domId:'hid-parentId',name:'parentId',defaultValue:0})
	  		,new FDTextBox({domId:'txt-groupName',name:'groupName',msgId:'formMsg_'
	  			,validates:[
	  		     	{rule:{notNull:true},successClass:'green',errorClass:'require-red',errorMsg:'用户组名称不能为空'}
	  		     	,{rule:{maxLength:20},successClass:'green',errorClass:'require-red',errorMsg:'用户组名称长度不能超过20'}
	  		     ]
	  		})
	  	]
	  });

FDRight.checkByCode('optRoot',function(){
	$('#btnAddRoot').click(function(){
		addRootMenu();
	});
});

new FDButton({domId:'btnSave',text:'保存',operateCode:'save',onclick:function(){
	saveGroup();
}})


groupTree = new FDTree({
	domId:'groupTree'
	,url:listAllGroupUrl
	,valueFieldName:'groupId'
	,textFieldName:'groupName'
	,childrenFieldName:'children'
	,onclick:function(node) {
		bindData(node);
	}
	,render:function(node){
		var text = node.groupName ;
		text += '&nbsp;' + buildTreeButton(node);
		return text;
	}
});

groupTree.expandAll();

// 初始化用户组用户
initGroupUser();
// 初始化角色
initGroupRole();

var regEn = /^[a-zA-Z]{1,20}$/;
FDValidateStore.wholeEN = function(val){
	return regEn.test(val);
}

function buildTreeButton(node){
	var html = [];
	FDRight.checkByCode('optRoot',function(){
		html.push('<a onclick="'+FunUtil.createFun(window,'addChildNode',node)+' return false;">[添加子节点]</a>');
		if(node.children.length == 0) {
			html.push('&nbsp;<a onclick="'+FunUtil.createFun(window,'delGroup',node)+' return false;">[删除节点]</a>');
		}
	});
	return html.join('');
}

function addRootMenu(){
	initAddNodeWin();
	addNodeWin.setTitle('添加根节点');
	addNodeForm.reset();
	addNodeWin.show();
}

window.addChildNode = function(node){
	initAddNodeWin();
	addNodeWin.setTitle('添加['+node.groupName+']子节点');
	addNodeForm.reset();
	
	addNodeForm.getControl('parentId').setValue(node.groupId);
	
	addNodeWin.show();
}

function initAddNodeWin() {
	if(!addNodeWin){
		addNodeWin = new FDWindow({
			contentId:'addNodeWin'
			,width:'500px'
			,buttons:[
				{text:'保存',onclick:function(){
					saveNode();
				}}
				,{text:'取消',onclick:function(){
					addNodeWin.close();
				}}
			]
		});
	}
}

function saveNode() {
	var url = addGroupUrl;
	
	if(addNodeForm.validate()) {
		Action.post(url,addNodeForm.getData(),function(result){
			Action.execResult(result,function(result){
				addNodeWin.close();
				reloadTree();
			});
		});
	}
}

function reloadTree() {
	groupTree.reload();
	groupTree.expandAll();
}

window.delGroup = function(node){
	var self = this;
	if (node) {
		var msg = '确定要删除<strong>'+node.groupName+'</strong>吗?'+
				'<br><span class="red">角色及权限将一并删除</span>';
		FDWindow.confirm(msg,function(r){
			if (r){
				Action.post(delRGroupUrl,{groupId:node.groupId,groupName:node.groupName},function(result){
					Action.execResult(result,function(result){
						reloadTree();
						$rightTip.show();
						$rightCont.hide();
						FDWindow.alert('删除成功');
					});
				});
			}
		});
	}
}

function bindData(node) {
	updateNodeForm.setData(node);
	
	tab.selectItemByValue(1);
	
	searchUser();
	searchRole();
	
	$rightTip.hide();
	$rightCont.show();
}

function saveGroup(){
	if(updateNodeForm.validate()) {
		var data = updateNodeForm.getData();
		Action.post(updateGroupUrl,data,function(result){
			Action.execResult(result,function(result){
				reloadTree();
				FDWindow.alert('修改成功');
			});
		});
	}	
}

