// -----------用户组成员------------
var gridUser;
var searchUserForm;

var delUserUrl = ctx + 'delRGroupUser.do';
var listUserUrl = ctx + 'listRGroupUser.do';

function initGroupUser() {
	if(!searchUserForm) {
		searchUserForm = new FDFormPanel({
			controls:[
				new FDTextBox({domId:'txt-usernameSch',name:'usernameSch'})
			]
		});
		
		new FDButton({domId:'btnUserSch',text:'搜索',onclick:function(){
			searchUser();
		}})
		
		FDRight.checkByCode('addGroupUser',function(){
			$('#addUser').click(function(){
				searchNOAddUser();
				addUserWin.show();
			});
		});
		
		gridUser = new FDGrid({
			domId:'gridUser'
			,width:'600px'
			,id:'username'
			,loadSearch:false
			,url:listUserUrl
			,noDataText:'暂无成员加入'
			,columns:[
		  		{text:"用户名",name:"username"}
		  	]
			,actionButtons:[
				{text:'删除',operateCode:'removeGroupUser',onclick:function(rowData){
					var msg = '确定要移除<strong>'+rowData.username+'</strong>吗?';
					FDWindow.confirm(msg,function(r){
						if(r){
							doDelUser(rowData);
						}
					});
				}}
			]
		});
		
		initAddUserWin();
	}
}



function searchUser() {
	var data = searchUserForm.getData();
	var groupId = updateNodeForm.getData('groupId');
	data.groupIdSch = groupId;
	
	gridUser.search(data);
}

function doDelUser(row){
	Action.post(delUserUrl,row,function(result){
		Action.execResult(result,function(result){
			searchUser();
		});
	});
}

//--------添加用户组成员win-------
var noAddUserForm;
var gridAddUser;
var addUserWin;
var listAddUserUrl = ctx + 'listGroupNoAddUser.do';
var addGroupUserUrl = ctx + 'addGroupUser.do';

function initAddUserWin() {
	addUserWin = new FDWindow({
		contentId:'addUserWin'
		,width:'600px'
		,height:'450px'
		,title:'添加成员'
		,buttons:[
			{text:'加入',onclick:function(){
				addGroupUser();
			}}
			,{text:'取消',onclick:function(){
				addUserWin.close();
			}}
		]
	});
	
	noAddUserForm = new FDFormPanel({
		controls:[
			new FDTextBox({domId:'txt-usernameNoAddSch',name:'usernameSch'})
		]
	});
	
	new FDButton({domId:'btnAddUserSch',text:'查询',onclick:function(){
		searchNOAddUser();
	}})
	
	gridAddUser = new FDGrid({
		domId:'gridAddUser'
		,width:'500px'
		,id:'username'
		,loadSearch:false
		,url:listAddUserUrl
		,selectOption:{multiSelect:true,cache:true,onclick:chekcUserHandler}
		,columns:[
	  		{text:"用户名",name:"username"}
	  	]
	});
	
}

function searchNOAddUser() {
	var data = noAddUserForm.getData();
	var groupId = updateNodeForm.getData('groupId');
	data.groupIdSch = groupId;
	
	gridAddUser.search(data);
	
	resetAddUser()
}

function resetAddUser() {
	noAddUserForm.reset();
	addUserCnt.innerHTML='0';
}

var addUserCnt = FDLib.getEl('addUserCnt');
function chekcUserHandler(rowData,selectDom,rowIndex,tr) {
	
	addUserCnt.innerHTML = gridAddUser.getChecked().length;
}

function addGroupUser() {
	var rows = gridAddUser.getChecked();
	var usernames = [];
	if(rows && rows.length > 0){
		for(var i=0,len=rows.length;i<len;i++){
			usernames.push(rows[i].username);
		}
		var groupId = updateNodeForm.getData('groupId');
		var params = {usernames:usernames,groupId:groupId}
		Action.post(addGroupUserUrl,params ,function(e){
			if(e.success) {
				addUserWin.close();
				gridUser.refresh();
			}else{
				FDWindow.alert('添加失败');
			}
		});
		
	}else{
		FDWindow.alert('请选择成员');
	}
	
}
