package org.durcframework.rms.service;

import org.durcframework.core.service.CrudService;
import org.durcframework.rms.dao.RDataTypeDao;
import org.durcframework.rms.entity.RDataType;
import org.springframework.stereotype.Service;

@Service
public class RDataTypeService extends CrudService<RDataType, RDataTypeDao> {

}
