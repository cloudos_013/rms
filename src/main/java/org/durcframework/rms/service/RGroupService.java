package org.durcframework.rms.service;

import org.durcframework.core.expression.ExpressionQuery;
import org.durcframework.core.expression.subexpression.ValueExpression;
import org.durcframework.core.service.CrudService;
import org.durcframework.rms.dao.RGroupDao;
import org.durcframework.rms.entity.RGroup;
import org.springframework.stereotype.Service;

@Service
public class RGroupService extends CrudService<RGroup, RGroupDao> {
	
	/**
	 * 判断是否有子节点
	 * @param group
	 * @return
	 */
	public boolean hasChild(RGroup group){
		ExpressionQuery query = new ExpressionQuery();
		query.add(new ValueExpression("parent_id", group.getGroupId()));
		int count = this.findTotalCount(query);
		
		return count > 0;
	}
	
	/**
	 * 删除用户组
	 * 步骤:1.删除角色相关;2.删除用户组相关;
	 */
	@Override
	public void del(RGroup entity) {
		int groupId = entity.getGroupId();
		
		this.getDao().delRolesByGroupId(groupId);
		
		this.getDao().delGroupByGroupId(groupId);		
	}
}