package org.durcframework.rms.service;

import java.util.ArrayList;
import java.util.List;

import org.durcframework.core.expression.ExpressionQuery;
import org.durcframework.core.expression.QBC;
import org.durcframework.core.expression.subexpression.ValueExpression;
import org.durcframework.core.service.CrudService;
import org.durcframework.rms.common.UserMenu;
import org.durcframework.rms.dao.RSysResDao;
import org.durcframework.rms.entity.RSysRes;
import org.durcframework.rms.util.TreeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RSysResService extends CrudService<RSysRes, RSysResDao> {
	@Autowired
	private RSysFunctionService functionService;
	
	/**
	 * 判断是否有子节点
	 * @param sysRes
	 * @return
	 */
	public boolean hasChild(RSysRes sysRes){
		ExpressionQuery query = new ExpressionQuery();
		query.add(new ValueExpression("parent_id", sysRes.getSrId()));
		int count = this.findTotalCount(query);
		
		return count > 0;
	}
	
	/**
	 * 根据用户名获取菜单
	 * @param username 用户名
	 * @return 返回用户菜单列表
	 */
	public List<UserMenu> getUserMenu(String username){
		
		List<RSysRes> list = this.getDao().findUserMenu(username);
		
		List<UserMenu> menus = new ArrayList<UserMenu>(list.size());
		
		UserMenu menu = null;
		for (RSysRes rSysRes : list) {
			menu = new UserMenu();
			menu.setMenuId(rSysRes.getId());
			menu.setText(rSysRes.getText());
			menu.setParentId(rSysRes.getParentId());
			menu.setUrl(rSysRes.getUrl());
			menus.add(menu);
		}
		
		menus = TreeUtil.buildTreeData(menus);
		
		return menus;
	}
	
	/**
	 * 全部菜单
	 * @return
	 */
	public List<RSysRes> getAllRSysRes(){
		return QBC.create(this.getDao()).listAll();
	}
	
	
	/**
	 * 删除资源
	 * 首先删除对应的系统功能,在删除自身
	 */
	@Override
	public void del(RSysRes entity) {
		functionService.delBySrId(entity.getSrId());
		this.getDao().del(entity);
	}
	
}
