package org.durcframework.rms.service;

import org.durcframework.core.service.SearchService;
import org.durcframework.rms.dao.ResOperateDao;
import org.durcframework.rms.entity.ResOperate;
import org.springframework.stereotype.Service;

@Service
public class ResOperateService extends SearchService<ResOperate, ResOperateDao> {

}
