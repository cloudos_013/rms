package org.durcframework.rms.service;

import java.util.List;

import org.durcframework.core.service.CrudService;
import org.durcframework.rms.constant.RoleType;
import org.durcframework.rms.dao.RGroupRoleDao;
import org.durcframework.rms.entity.RGroupRole;
import org.durcframework.rms.entity.RGroupUser;
import org.durcframework.rms.entity.RRole;
import org.durcframework.rms.entity.RUserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RGroupRoleService extends CrudService<RGroupRole, RGroupRoleDao> {

	@Autowired
	private RRoleService roleService;
	@Autowired
	private RUserRoleService userRoleService;
	@Autowired
	private RGroupUserService groupUserService;
	
	/**
	 * 获取用户组对应的角色ID
	 * @param groupId
	 * @return
	 */
	public List<Integer> getRoleIdsByGroupId(int groupId) {
		return this.getDao().getRoleIdsByGroupId(groupId);
	}
	
	/**
	 * 添加用户组角色
	 * 1. 添加角色表数据
	 * 2. 添加用户组角色表数据
	 * 3. 添加用户角色表数据
	 * @param groupId
	 * @param roleName
	 */
	public void addGroupRole(int groupId,String roleName) {
		// 1
		RRole role = new RRole();
		role.setRoleName(roleName);
		role.setRoleType(RoleType.GROUP);
		roleService.save(role);
		// 2
		RGroupRole groupRole = new RGroupRole();
		groupRole.setRoleId(role.getRoleId());
		groupRole.setGroupId(groupId);
		
		this.save(groupRole);
		// 3
		this.setGroupUserRole(groupId, role.getRoleId());
	}
	
	/**
	 * 添加用户角色
	 * @param groupId
	 * @param roleId
	 */
	public void setGroupUserRole(int groupId,int roleId) {
		List<RGroupUser> groupUsers = groupUserService.getGroupUserByGroupId(groupId);
		
		for (RGroupUser groupUser : groupUsers) {
			RUserRole userRole = new RUserRole();
			userRole.setRoleId(roleId);
			userRole.setRoleType(RoleType.GROUP);
			userRole.setUsername(groupUser.getUsername());
			userRoleService.save(userRole);
		}
	}
	
	/**
	 * 删除用户组角色
	 * 先删除角色表,在删除用户组角色表
	 */
	@Override
	public void del(RGroupRole groupRole) {
		RRole role = new RRole();
		role.setRoleId(groupRole.getRoleId());
		roleService.del(role);
		
		this.getDao().del(groupRole);
	}
	
}