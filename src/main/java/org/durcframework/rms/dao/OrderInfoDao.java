package org.durcframework.rms.dao;

import org.durcframework.core.dao.BaseDao;
import org.durcframework.rms.entity.OrderInfo;

public interface OrderInfoDao extends BaseDao<OrderInfo> {
}