package org.durcframework.rms.dao;

import org.durcframework.core.dao.BaseDao;
import org.durcframework.rms.entity.ResOperate;

public interface ResOperateDao extends BaseDao<ResOperate> {

}
