package org.durcframework.rms.dao;

import java.util.List;

public class AddGroupUserPojo {

	private int groupId;
	private List<String> usernames;

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public List<String> getUsernames() {
		return usernames;
	}

	public void setUsernames(List<String> usernames) {
		this.usernames = usernames;
	}

}
