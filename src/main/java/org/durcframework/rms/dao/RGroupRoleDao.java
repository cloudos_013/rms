package org.durcframework.rms.dao;

import java.util.List;

import org.durcframework.core.dao.BaseDao;
import org.durcframework.rms.entity.RGroupRole;

public interface RGroupRoleDao extends BaseDao<RGroupRole> {
	List<Integer> getRoleIdsByGroupId(int groupId);
}