package org.durcframework.rms.dao;

import org.durcframework.core.dao.BaseDao;
import org.durcframework.rms.entity.RGroupUser;

public interface RGroupUserDao extends BaseDao<RGroupUser> {
	void addGroupUser(AddGroupUserPojo addGroupUserPojo);
	void delGroupUser(RGroupUser groupUser);
}