package org.durcframework.rms.dao;

import org.durcframework.core.dao.BaseDao;
import org.durcframework.rms.entity.RDataType;

public interface RDataTypeDao extends BaseDao<RDataType> {
}