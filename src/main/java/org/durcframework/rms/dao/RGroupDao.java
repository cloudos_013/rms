package org.durcframework.rms.dao;

import org.apache.ibatis.annotations.Param;
import org.durcframework.core.dao.BaseDao;
import org.durcframework.rms.entity.RGroup;

public interface RGroupDao extends BaseDao<RGroup> {
	void delRolesByGroupId(@Param("groupId") int groupId);
	void delGroupByGroupId(@Param("groupId") int groupId);
}