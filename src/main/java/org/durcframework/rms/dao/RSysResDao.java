package org.durcframework.rms.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.durcframework.core.dao.BaseDao;
import org.durcframework.rms.entity.RSysRes;

public interface RSysResDao extends BaseDao<RSysRes> {
	List<RSysRes> findUserMenu(@Param("username") String username);
}