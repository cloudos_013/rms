package org.durcframework.rms.entity;

public class RGroupUser {
	private int groupId;
	private String username;

	public void setGroupId(int groupId){
		this.groupId = groupId;
	}

	public int getGroupId(){
		return this.groupId;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return this.username;
	}

}