package org.durcframework.rms.entity;

import org.durcframework.rms.constant.RoleType;

public class RUserRole {
	private String username;
	private int roleId;
	private byte roleType = RoleType.PERSON;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getRoleId() {
		return this.roleId;
	}

	public byte getRoleType() {
		return roleType;
	}

	public void setRoleType(byte roleType) {
		this.roleType = roleType;
	}

}