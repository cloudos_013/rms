package org.durcframework.rms.entity;

import java.util.List;

import org.durcframework.rms.constant.RoleType;

public class UserRoleParam {
	private String username;
	private List<Integer> roleIds;
	private byte roleType = RoleType.PERSON;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Integer> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(List<Integer> roleIds) {
		this.roleIds = roleIds;
	}

	public byte getRoleType() {
		return roleType;
	}

	public void setRoleType(byte roleType) {
		this.roleType = roleType;
	}

}
