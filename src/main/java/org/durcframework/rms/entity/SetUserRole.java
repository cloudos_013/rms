package org.durcframework.rms.entity;

import java.util.List;

public class SetUserRole {
	private String username;
	private List<Integer> groupRoleIds;
	private List<Integer> personRoleIds;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Integer> getGroupRoleIds() {
		return groupRoleIds;
	}

	public void setGroupRoleIds(List<Integer> groupRoleIds) {
		this.groupRoleIds = groupRoleIds;
	}

	public List<Integer> getPersonRoleIds() {
		return personRoleIds;
	}

	public void setPersonRoleIds(List<Integer> personRoleIds) {
		this.personRoleIds = personRoleIds;
	}

}
