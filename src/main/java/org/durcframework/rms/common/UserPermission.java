package org.durcframework.rms.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// key/value -> srId/List<UserOperation>
public class UserPermission extends HashMap<String, List<UserOperation>> {
	private static final long serialVersionUID = 1L;
	
	private List<String> urls = new ArrayList<String>();

	private static boolean isEmpty(Object str) {
		return (str == null || "".equals(str));
	}

	public List<UserOperation> getBySrId(String srId) {
		return this.get(srId);
	}
	
	/**
	 * 添加菜单权限
	 * 
	 * @param srId
	 *            菜单ID
	 * @param operateCode
	 *            权限代码
	 */
	public void addPermission(String srId, UserOperation userOperation) {
		List<UserOperation> userOperations = this.get(srId);
		if (userOperations == null) {
			userOperations = new ArrayList<UserOperation>();
			this.put(srId, userOperations);
		}
		userOperations.add(userOperation);
		
		this.addUrl(userOperation.getUrl());
	}
	
	/**
	 * 添加用户url
	 * @param url
	 */
	public void addUrl(String url) {
		urls.add(url);
	}
	
	/**
	 * 验证用户url
	 * @param username
	 * @param url
	 * @return
	 */
	public boolean isValidUrl(String url) {
		return urls.contains(url);
	}

	/**
	 * 菜单srId是否具有某operateCode权限
	 * 
	 * @param srId
	 * @param operateCode
	 * @return true,有
	 */
	public boolean hasPermission(String srId, String operateCode) {
		if (isEmpty(srId) || isEmpty(operateCode)) {
			return false;
		}

		List<UserOperation> userOperations = this.get(srId);
		
		if(userOperations == null) {
			return false;
		}
		
		for (UserOperation userOperation : userOperations) {
			if(operateCode.equals(userOperation.getOperateCode())) {
				return true;
			}
		}

		return false;
	}
	

}
