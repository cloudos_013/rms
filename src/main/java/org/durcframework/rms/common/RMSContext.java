package org.durcframework.rms.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.durcframework.core.SpringContext;
import org.durcframework.core.UserContext;
import org.durcframework.core.WebContext;
import org.durcframework.core.expression.Expression;
import org.durcframework.rms.entity.RUser;
import org.durcframework.rms.entity.RUserRole;
import org.durcframework.rms.service.RDataPermissionService;
import org.durcframework.rms.service.RSysFunctionService;
import org.durcframework.rms.service.RSysResService;
import org.durcframework.rms.service.RUserRoleService;
import org.springframework.util.StringUtils;

public enum RMSContext {
	INS;

	private static final String USER_ROLE_IDS = "user_role_ids";

	private static UserMenuContext userMenuContext = new UserMenuContext();
	private static UserPermissionContext permissionContext = new UserPermissionContext();
	
	public static RMSContext getInstance() {
		return INS;
	}

	
	/**
	 * 获取当前用户权限
	 * @return
	 */
	public UserPermission getCurrentUserPermission() {
		RUser user = UserContext.getInstance().getUser();
		if(user == null) {
			return new UserPermission();
		}
		return this.getUserPermission(user.getUsername());
	}
	
	public UserPermission getUserPermission(String username) {
		return permissionContext.get(username);
	}
	
	
	/**
	 * 刷新保存用权限数据.(系统功能=菜单+操作点)
	 */
	public void refreshUserRightData(String username){
		RSysFunctionService sysFunctionService = SpringContext.getBean(RSysFunctionService.class);
		
		UserPermission userPermission = sysFunctionService.buildUserPermission(username);
		
		permissionContext.put(username, userPermission);
		
		//userSysFunctionMap.put(username, userSysFuns);
		
		saveUserRoleIds(username);
		
		refreshUserMenu(username);
	}
	
	/**
	 * 刷新用户菜单
	 * @param username
	 */
	public void refreshUserMenu(String username) {
		RSysResService sysResService =SpringContext.getBean(RSysResService.class);
		List<UserMenu> userMenu = sysResService.getUserMenu(username);
		userMenuContext.put(username, userMenu);
	}
	
	/**
	 * 获取用户菜单
	 * @param username
	 * @return
	 */
	public List<UserMenu> getUserMenu() {
		RUser user = UserContext.getInstance().getUser();
		return userMenuContext.get(user.getUsername());
	}
	
	/**
	 * 获取用户数据权限条件
	 * @return
	 */
	public List<Expression> getUserDataExpressions() {
		String srId = WebContext.getInstance().getRequest().getParameter("srId");
		
		if(srId == null){
			return Collections.emptyList();
		}
    	List<Integer> roleIds = getCurrentUserRoleIds();
    	RDataPermissionService dataPermissionService = SpringContext.getBean(RDataPermissionService.class);
    	
    	return dataPermissionService.buildDataExpresstions(roleIds, Integer.valueOf(srId));
	}
	
	/**
	 * 保存用户角色ID
	 * @param username
	 */
	public void saveUserRoleIds(String username){
		RUserRoleService userRoleService = SpringContext.getBean(RUserRoleService.class);
		List<RUserRole> userRoles = userRoleService.getUserRole(username);
		
		if(CollectionUtils.isNotEmpty(userRoles)){
			List<Integer> roleIds = new ArrayList<Integer>(userRoles.size());
			for (RUserRole userRole : userRoles) {
				roleIds.add(userRole.getRoleId());
			}
			WebContext.getInstance().setAttr(USER_ROLE_IDS, roleIds);
		}
	}
	
	/**
	 * 获取当前用户角色ID
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Integer> getCurrentUserRoleIds() {
		Object roleIds = WebContext.getInstance().getAttr(USER_ROLE_IDS);
		
		if(roleIds == null){
			return Collections.emptyList();
		}
		
		return (List<Integer>)roleIds;
	}
	
	/**
	 * 刷新所有用户的系统功能
	 */
	public void refreshAllUserRightData(){
		Set<String> usernameSet = permissionContext.keySet();
		for (String username : usernameSet) {
			this.refreshUserRightData(username);
		}
	}
	
	/**
	 * 移除用户权限数据,在用户注销或session失效可以用到
	 * @param username
	 */
	public void clearUserRightData(String username){
		if(StringUtils.isEmpty(username)){
			return;
		}
		permissionContext.remove(username);
		userMenuContext.remove(username);
	}
	
	/**
	 * 移除当前用户权限数据
	 */
	public void clearCurrentUserRightData(){
		RUser user = UserContext.getInstance().getUser();
		if(user != null){
			this.clearUserRightData(user.getUsername());
		}
	}
}
