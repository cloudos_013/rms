package org.durcframework.rms.util;

import org.durcframework.rms.common.RMSContext;
import org.durcframework.rms.common.UserPermission;

/**
 * 权限检查工具类
 *
 */
public class RightUtil {

	/**
	 * 根据资源ID和操作代码检查是否具有权限
	 * 
	 * @param srId SysRec表主键
	 * @param operateCode SysOperate表主键
	 * @return 返回true权限存在
	 */
	public static boolean checkOperateCode(String srId, String operateCode) {
		UserPermission userPermission = RMSContext.getInstance().getCurrentUserPermission();
		
		return userPermission.hasPermission(srId, operateCode);
	}
	
	/**
	 * 根据username和URL检查是否具有权限
	 * @param username
	 * @param url
	 * @return 
	 */
	public static boolean checkCurrentUserUrl(String username,String url) {
		UserPermission userPermission = RMSContext.getInstance().getUserPermission(username);
		if(userPermission == null) {
			return false;
		}
		return userPermission.isValidUrl(url);
		//return true;
	}
}
