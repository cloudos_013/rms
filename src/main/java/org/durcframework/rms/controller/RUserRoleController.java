package org.durcframework.rms.controller;

import org.durcframework.core.controller.SearchController;
import org.durcframework.core.expression.ExpressionQuery;
import org.durcframework.core.expression.subexpression.ValueExpression;
import org.durcframework.rms.entity.RUserRole;
import org.durcframework.rms.entity.SetUserRole;
import org.durcframework.rms.service.RUserRoleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RUserRoleController extends
		SearchController<RUserRole, RUserRoleService> {

	/**
	 * 获取用户所有角色
	 * 
	 * @param searchEntity
	 * @return
	 */
	@RequestMapping("/listUserRRole.do")
	public ModelAndView listUserRRole(
			@RequestParam("usernameSch") String usernameSch) {
		ExpressionQuery query = ExpressionQuery.buildQueryAll();
		query.add(new ValueExpression("username", usernameSch));
		return this.listAll(query);
	}

	/**
	 * 设置角色
	 * 
	 * @param setUserRole
	 * @return
	 */
	@RequestMapping("/setUserRoles.do")
	public ModelAndView setUserRoles(SetUserRole setUserRole) {
		this.getService().setUserRoles(setUserRole);
		return successView();
	}
}
