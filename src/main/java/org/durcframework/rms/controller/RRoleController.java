package org.durcframework.rms.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.durcframework.core.controller.CrudController;
import org.durcframework.rms.entity.RRole;
import org.durcframework.rms.entity.RRolePermission;
import org.durcframework.rms.entity.RRoleSch;
import org.durcframework.rms.entity.RUserRole;
import org.durcframework.rms.entity.SetRoleParam;
import org.durcframework.rms.service.RRolePermissionService;
import org.durcframework.rms.service.RRoleService;
import org.durcframework.rms.service.RUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RRoleController extends CrudController<RRole, RRoleService> {

	@Autowired
	private RUserRoleService userRoleService;
	@Autowired
	private RRolePermissionService rolePermissionService;

	@RequestMapping("/addRRole.do")
	public ModelAndView addRRole(SetRoleParam param) {
		if (StringUtils.hasText(param.getRoleName())) {
			RRole role = new RRole();
			role.setRoleName(param.getRoleName());
			this.getService().addRole(role, param.getSfId());
			return successView();
		}
		return errorView("添加失败");
	}

	@RequestMapping("/listRolePermissionByRoleId.do")
	public ModelAndView listRolePermissionByRoleId(int roleId) {
		List<RRolePermission> list = rolePermissionService.getRolePermissionByRole(roleId);
		return this.render(list);
	}

	@RequestMapping("/listRRole.do")
	public ModelAndView listRRole(RRoleSch searchEntity) {
		return this.list(searchEntity);
	}

	@RequestMapping("/updateRRole.do")
	public ModelAndView updateRRole(SetRoleParam param) {
		if (StringUtils.hasText(param.getRoleName())) {
			RRole role = new RRole();
			role.setRoleId(param.getRoleId());
			role.setRoleName(param.getRoleName());
			this.getService().updateRole(role, param.getSfId());
			return successView();
		}
		return errorView("修改失败");
	}

	@RequestMapping("/delRRole.do")
	public ModelAndView delRRole(RRole enity) {
		return this.remove(enity);
	}

	@RequestMapping("/listRoleRelationInfo.do")
	public ModelAndView listRoleRelationInfo(int roleId) {

		List<RUserRole> userRoles = userRoleService.getUserRoleByRoleId(roleId);
		Map<String, Object> retMap = new HashMap<String, Object>();

		retMap.put("userRoles", userRoles);
		retMap.put("success", true);

		return this.render(retMap);
	}
	
	@RequestMapping("/listAllRRole_backuser.do")
	public ModelAndView listAllRRole(RRoleSch searchEntity) {
		return this.listAll(searchEntity);
	}

}