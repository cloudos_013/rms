package org.durcframework.rms.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.durcframework.core.controller.CrudController;
import org.durcframework.core.expression.ExpressionQuery;
import org.durcframework.core.expression.subexpression.ValueExpression;
import org.durcframework.rms.entity.AddOperateParam;
import org.durcframework.rms.entity.RSysFunction;
import org.durcframework.rms.entity.RSysRes;
import org.durcframework.rms.service.RRolePermissionService;
import org.durcframework.rms.service.RSysFunctionService;
import org.durcframework.rms.service.RSysResService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RSysFunctionController extends
		CrudController<RSysFunction, RSysFunctionService> {
	@Autowired
	private RSysResService resService;
	@Autowired
	private RRolePermissionService permissionService;

	// 根据资源ID获取操作权限
	@RequestMapping("/listSysFunctionBySrId.do")
	public ModelAndView listSysFunctionBySrId(
			@RequestParam(value = "srId", required = true, defaultValue = "0") int srId) {
		ExpressionQuery query = ExpressionQuery.buildQueryAll();
		query.add(new ValueExpression("sr_id", srId));
		return this.list(query);
	}

	/**
	 * 添加操作权限
	 * 
	 * @param sysFunction
	 * @return
	 */
	@RequestMapping("/addSysFunction.do")
	public ModelAndView addSysFunction(RSysFunction sysFunction) {
		RSysRes res = resService.get(sysFunction.getSrId());
		if (res == null) {
			return errorView("资源不存在");
		}

		if (this.getService().isExistSysFun(sysFunction.getOperateCode(),
				sysFunction.getSrId())) {
			return errorView("操作点已添加");
		}
		return this.add(sysFunction);
	}

	@RequestMapping("/setSysFunctionRole.do")
	public ModelAndView setSysFunctionRole(AddOperateParam addOperateParam,
			int sfId) {
		this.permissionService.setSysFunctionRole(sfId,
				addOperateParam.getRoleId());
		return successView();
	}

	@RequestMapping("/delRSysFunction.do")
	public ModelAndView delRSysFunction(RSysFunction sysFunction) {
		return this.remove(sysFunction);
	}

	@RequestMapping("/listOperateUse.do")
	public ModelAndView listOperateUse(String operateCode) {

		List<RSysFunction> list = this.getService().getByOperateCode(
				operateCode);

		Map<String, Object> map = new HashMap<String, Object>();
		boolean operateCodeUsed = CollectionUtils.isNotEmpty(list);

		if (operateCodeUsed) {
			for (RSysFunction sysFun : list) {
				RSysRes res = resService.get(sysFun.getSrId());
				sysFun.setResName(res.getResName());
			}
		}

		map.put("operateCodeUsed", operateCodeUsed);
		map.put("operateCodeUsedList", list);

		return this.render(map);
	}

}