package org.durcframework.rms.controller;

import java.util.Date;

import org.durcframework.rms.common.DataPermissionController;
import org.durcframework.rms.entity.OrderInfo;
import org.durcframework.rms.entity.OrderInfoSch;
import org.durcframework.rms.service.OrderInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class OrderInfoController extends
		DataPermissionController<OrderInfo, OrderInfoService> {

	@RequestMapping("/addOrderInfo.do")
	public ModelAndView addOrderInfo(OrderInfo entity) {
		entity.setCreateDate(new Date());
		return this.add(entity);
	}

	@RequestMapping("/listOrderInfo.do")
	public ModelAndView listOrderInfo(OrderInfoSch searchEntity) {
		return this.list(searchEntity);
	}

	@RequestMapping("/updateOrderInfo.do")
	public ModelAndView updateOrderInfo(OrderInfo enity) {
		return this.modify(enity);
	}

	@RequestMapping("/delOrderInfo.do")
	public ModelAndView delDataSource(OrderInfo enity) {
		return this.remove(enity);
	}

}