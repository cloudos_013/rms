package org.durcframework.rms.controller;

import org.durcframework.core.controller.BaseController;
import org.durcframework.rms.common.RMSContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SystemController extends BaseController {
	// 刷新缓存
	@RequestMapping("refreshCache.do")
	public ModelAndView refreshCache() {
		RMSContext.getInstance().refreshAllUserRightData();
		return this.successView();
	}
}
