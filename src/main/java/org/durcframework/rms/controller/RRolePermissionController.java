package org.durcframework.rms.controller;

import java.util.List;

import org.durcframework.core.controller.CrudController;
import org.durcframework.rms.entity.RRolePermission;
import org.durcframework.rms.service.RRolePermissionService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RRolePermissionController extends
		CrudController<RRolePermission, RRolePermissionService> {

	@RequestMapping("listRolePermissionSfId_backuser.do")
	public ModelAndView listRolePermissionSfId(int sfId) {
		List<RRolePermission> list = this.getService().getRolePermissionBySfId(sfId);
		return this.render(list);
	}
}
