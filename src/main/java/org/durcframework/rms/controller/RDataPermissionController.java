package org.durcframework.rms.controller;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.durcframework.core.JsonObjProcessor;
import org.durcframework.core.controller.CrudController;
import org.durcframework.rms.entity.RDataPermission;
import org.durcframework.rms.entity.RDataPermissionSch;
import org.durcframework.rms.entity.RRole;
import org.durcframework.rms.entity.RSysRes;
import org.durcframework.rms.service.RDataPermissionService;
import org.durcframework.rms.service.RRoleService;
import org.durcframework.rms.service.RSysResService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RDataPermissionController extends
		CrudController<RDataPermission, RDataPermissionService> {

	@Autowired
	private RRoleService roleService;
	@Autowired
	private RSysResService resService;

	@RequestMapping("/addRDataPermission.do")
	public ModelAndView addRDataPermission(RDataPermission entity) {
		RSysRes res = resService.get(entity.getSrId());
		if (res == null) {
			return errorView("资源不存在");
		}
		if (CollectionUtils.isEmpty(entity.getRoleId())) {
			return errorView("请选择角色");
		}

		this.getService().saveDataPermission(entity);

		return successView();
	}

	@RequestMapping("/listRDataPermission.do")
	public ModelAndView listRDataPermission(RDataPermissionSch searchEntity) {
		return this.listWithProcessor(searchEntity,
				new JsonObjProcessor<RDataPermission>() {
					@Override
					public void process(RDataPermission entity,
							Map<String, Object> jsonObject) {
						List<RRole> roles = roleService
								.getDataPermissionRole(entity);
						jsonObject.put("roles", roles);
					}
				});
	}

	@RequestMapping("/updateRDataPermission.do")
	public ModelAndView updateRDataPermission(RDataPermission entity) {
		RSysRes res = resService.get(entity.getSrId());
		if (res == null) {
			return errorView("资源不存在");
		}
		if (CollectionUtils.isEmpty(entity.getRoleId())) {
			return errorView("请选择角色");
		}

		this.getService().updateDataPermission(entity);

		return successView();
	}

	@RequestMapping("/delRDataPermission.do")
	public ModelAndView delRDataPermission(RDataPermission entity) {
		this.getService().delDataPermission(entity);

		return successView();
	}

}