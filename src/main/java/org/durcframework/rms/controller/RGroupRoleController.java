package org.durcframework.rms.controller;

import org.durcframework.core.controller.CrudController;
import org.durcframework.core.expression.ExpressionQuery;
import org.durcframework.core.expression.subexpression.ValueExpression;
import org.durcframework.rms.entity.RGroupRole;
import org.durcframework.rms.entity.RGroupRoleSch;
import org.durcframework.rms.service.RGroupRoleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RGroupRoleController extends
		CrudController<RGroupRole, RGroupRoleService> {

	@RequestMapping("/listRoleByGroupId.do")
	public ModelAndView listRoleByGroupId(
			@RequestParam(value = "groupId", required = true, defaultValue = "0") int groupId) {
		ExpressionQuery query = ExpressionQuery.buildQueryAll();
		query.add(new ValueExpression("group_id", groupId));
		return this.list(query);
	}

	@RequestMapping("/addRGroupRole.do")
	public ModelAndView addRGroupRole(int groupId, String roleName) {
		this.getService().addGroupRole(groupId, roleName);
		return this.successView();
	}

	@RequestMapping("/listRGroupRole.do")
	public ModelAndView listRGroupRole(RGroupRoleSch searchEntity) {
		return this.list(searchEntity);
	}

	@RequestMapping("/delRGroupRole.do")
	public ModelAndView delRGroupRole(RGroupRole entity) {
		return this.remove(entity);
	}

}