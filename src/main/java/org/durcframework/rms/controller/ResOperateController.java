package org.durcframework.rms.controller;

import org.durcframework.core.controller.SearchController;
import org.durcframework.core.expression.ExpressionQuery;
import org.durcframework.rms.entity.ResOperate;
import org.durcframework.rms.entity.SysOperateSch;
import org.durcframework.rms.service.ResOperateService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ResOperateController extends
		SearchController<ResOperate, ResOperateService> {

	@RequestMapping("listResOperate.do")
	public ModelAndView listResOperate(SysOperateSch searchEntity) {
		ExpressionQuery query = this.buildExpressionQuery(searchEntity);
		query.addParam("srId", searchEntity.getSrIdSch());
		return this.list(query);
	}

}
