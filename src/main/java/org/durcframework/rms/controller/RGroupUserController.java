package org.durcframework.rms.controller;

import org.durcframework.core.controller.CrudController;
import org.durcframework.rms.dao.AddGroupUserPojo;
import org.durcframework.rms.entity.RGroupUser;
import org.durcframework.rms.entity.RGroupUserSch;
import org.durcframework.rms.service.RGroupUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RGroupUserController extends
		CrudController<RGroupUser, RGroupUserService> {

	@RequestMapping("/addGroupUser.do")
	public ModelAndView addGroupUser(AddGroupUserPojo addGroupUserPojo) {
		this.getService().addGroupUser(addGroupUserPojo);
		return this.successView();
	}

	@RequestMapping("/listRGroupUser.do")
	public ModelAndView listRGroupUser(RGroupUserSch searchEntity) {
		return this.list(searchEntity);
	}

	@RequestMapping("/delRGroupUser.do")
	public ModelAndView delRGroupUser(RGroupUser entity) {
		return this.remove(entity);
	}

}