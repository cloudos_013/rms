package org.durcframework.rms.controller;

import java.util.List;
import java.util.Map;

import org.durcframework.core.JsonObjProcessor;
import org.durcframework.core.UserContext;
import org.durcframework.core.controller.CrudController;
import org.durcframework.core.expression.ExpressionQuery;
import org.durcframework.core.expression.subexpression.DefaultJoinExpression;
import org.durcframework.core.expression.subexpression.SqlExpression;
import org.durcframework.rms.entity.RRole;
import org.durcframework.rms.entity.RUser;
import org.durcframework.rms.entity.RUserSch;
import org.durcframework.rms.service.RRoleService;
import org.durcframework.rms.service.RUserService;
import org.durcframework.rms.util.PasswordUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RUserController extends CrudController<RUser, RUserService> {

	@Autowired
	private RRoleService roleService;

	/**
	 * 获取待添加的用户组成员
	 * 
	 * @param groupId
	 * @return
	 */
	@RequestMapping("/listGroupNoAddUser.do")
	public ModelAndView listGroupNoAddUser(
			RUserSch searchEntity,
			@RequestParam(value = "groupIdSch", required = true, defaultValue = "0") int groupIdSch) {
		ExpressionQuery query = this.buildExpressionQuery(searchEntity);

		query.addJoinExpression(new DefaultJoinExpression(
				"LEFT JOIN r_group_user gu ON t.username = gu.username"));
		query.add(new SqlExpression("gu.group_id <> " + groupIdSch
				+ " OR gu.group_id IS NULL"));

		return this.list(query);
	}

	@RequestMapping("/addRUser.do")
	public ModelAndView addRUser(RUser entity) {
		String password = entity.getPassword(); // md5加密后的
		password = PasswordUtil.createHash(password);
		entity.setPassword(password);
		return this.add(entity);
	}

	@RequestMapping("/listRUser.do")
	public ModelAndView listRUser(RUserSch searchEntity) {
		return this.listWithProcessor(searchEntity,
				new JsonObjProcessor<RUser>() {
					@Override
					public void process(RUser entity,
							Map<String, Object> jsonObject) {
						List<RRole> userRoles = roleService.getUserRole(entity
								.getUsername());
						jsonObject.put("roles", userRoles);
					}
				});
	}

	@RequestMapping("/updateRUser.do")
	public ModelAndView updateRUser(RUser enity) {
		return this.modify(enity);
	}

	@RequestMapping("/resetUserPassword.do")
	public ModelAndView resetUserPassword(RUser user) {
		String newPwsd = this.getService().resetUserPassword(user);
		return successView(newPwsd);
	}

	@RequestMapping("/updateUserPassword_backuser.do")
	public ModelAndView updateUserPassword(String oldPswd, String newPswd,
			String newPswd2) {

		if (!newPswd.equals(newPswd2)) {
			return errorView("两次输入的新密码不一样");
		}
		RUser user = UserContext.getInstance().getUser();
		RUser storeUser = this.getService().get(user.getUsername());

		boolean isPswdCorrect = PasswordUtil.validatePassword(oldPswd,
				storeUser.getPassword());

		if (!isPswdCorrect) {
			return errorView("原密码输入有误");
		}

		this.getService().updateUserPassword(storeUser, newPswd);

		return successView();
	}

}