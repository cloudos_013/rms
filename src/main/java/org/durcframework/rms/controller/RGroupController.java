package org.durcframework.rms.controller;

import java.util.List;

import org.durcframework.core.DefaultGridResult;
import org.durcframework.core.controller.CrudController;
import org.durcframework.core.expression.ExpressionQuery;
import org.durcframework.rms.entity.RGroup;
import org.durcframework.rms.service.RGroupService;
import org.durcframework.rms.util.TreeUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RGroupController extends CrudController<RGroup, RGroupService> {

	// 获取所有菜单
	@SuppressWarnings("unchecked")
	@RequestMapping("/listAllGroup.do")
	public ModelAndView listAllGroup() {

		ExpressionQuery query = ExpressionQuery.buildQueryAll();

		DefaultGridResult resultGrid = (DefaultGridResult) this.queryAll(query);
		List<RGroup> rows = (List<RGroup>) resultGrid.getRows();

		List<RGroup> list = TreeUtil.buildGroupData(rows);

		return this.render(list);
	}

	@RequestMapping("/addRGroup.do")
	public ModelAndView addRGroup(RGroup entity) {
		return this.add(entity);
	}

	@RequestMapping("/updateRGroup.do")
	public ModelAndView updateRGroup(RGroup entity) {
		return this.modify(entity);
	}

	@RequestMapping("/delRGroup.do")
	public ModelAndView delRGroup(RGroup entity) {
		if (this.getService().hasChild(entity)) {
			return errorView(entity.getGroupName() + "下含有子节点,不能删除.");
		}
		return this.remove(entity);
	}

}