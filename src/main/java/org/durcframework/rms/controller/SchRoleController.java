package org.durcframework.rms.controller;

import java.util.List;

import org.durcframework.core.controller.SearchController;
import org.durcframework.core.expression.QBC;
import org.durcframework.rms.entity.RGroup;
import org.durcframework.rms.entity.RRole;
import org.durcframework.rms.service.RGroupService;
import org.durcframework.rms.service.RRoleService;
import org.durcframework.rms.util.TreeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SchRoleController extends SearchController<RGroup, RGroupService> {

	@Autowired
	private RRoleService roleService;

	@RequestMapping("/listTreeRole_backuser.do")
	public ModelAndView listRRole() {
		List<RGroup> rows = QBC.create(this.getService().getDao()).listAll();

		List<RRole> roles = null;
		for (RGroup group : rows) {
			roles = roleService.getRolesByGroupId(group.getGroupId());
			group.setRoles(roles);
		}
		
		List<RGroup> groupData = TreeUtil.buildGroupData(rows);

		return this.render(groupData);
	}
	
}
