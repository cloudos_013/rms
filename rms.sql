/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.1.73-community : Database - rms
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rms` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `rms`;

/*Table structure for table `order_info` */

DROP TABLE IF EXISTS `order_info`;

CREATE TABLE `order_info` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(150) DEFAULT NULL,
  `mobile` varchar(150) DEFAULT NULL,
  `address` varchar(600) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `order_info` */

insert  into `order_info`(`order_id`,`city_name`,`mobile`,`address`,`create_date`) values (1,'杭州','13711111111','杭州XXX大街11号','2015-05-05 15:23:07'),(2,'杭州','13722222222','杭州XXX大街12号','2015-05-05 15:23:09'),(3,'北京','13712345678','杭州XXX大街33号','2015-05-05 15:23:20'),(9,'12','22','32','2015-05-26 14:00:22'),(11,'12','13777777222','222','2015-05-28 11:51:34');

/*Table structure for table `r_data_obj` */

DROP TABLE IF EXISTS `r_data_obj`;

CREATE TABLE `r_data_obj` (
  `do_id` int(11) NOT NULL AUTO_INCREMENT,
  `dt_id` int(11) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`do_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_data_obj` */

/*Table structure for table `r_data_permission` */

DROP TABLE IF EXISTS `r_data_permission`;

CREATE TABLE `r_data_permission` (
  `dp_id` int(11) NOT NULL AUTO_INCREMENT,
  `sr_id` int(11) NOT NULL,
  `expression_type` tinyint(4) NOT NULL,
  `column` varchar(50) NOT NULL,
  `equal` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL,
  `remark` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`dp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/*Data for the table `r_data_permission` */

insert  into `r_data_permission`(`dp_id`,`sr_id`,`expression_type`,`column`,`equal`,`value`,`remark`) values (1,19,1,'city_name','=','杭州','杭州售后只能查看杭州地区的订单'),(25,19,1,'city_name','=','上海','上海售后只能查看上海地区订单');

/*Table structure for table `r_data_permission_role` */

DROP TABLE IF EXISTS `r_data_permission_role`;

CREATE TABLE `r_data_permission_role` (
  `dp_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`dp_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_data_permission_role` */

insert  into `r_data_permission_role`(`dp_id`,`role_id`) values (1,5),(25,15);

/*Table structure for table `r_data_type` */

DROP TABLE IF EXISTS `r_data_type`;

CREATE TABLE `r_data_type` (
  `dt_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(50) NOT NULL,
  PRIMARY KEY (`dt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_data_type` */

/*Table structure for table `r_datatype_res` */

DROP TABLE IF EXISTS `r_datatype_res`;

CREATE TABLE `r_datatype_res` (
  `dt_id` int(11) NOT NULL,
  `sr_id` int(11) NOT NULL,
  PRIMARY KEY (`dt_id`,`sr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_datatype_res` */

/*Table structure for table `r_group` */

DROP TABLE IF EXISTS `r_group`;

CREATE TABLE `r_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(50) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `r_group` */

insert  into `r_group`(`group_id`,`group_name`,`parent_id`) values (8,'杭州分公司',0),(9,'上海分公司',0),(10,'财务部',8),(11,'财务部',9),(12,'综合部',8);

/*Table structure for table `r_group_role` */

DROP TABLE IF EXISTS `r_group_role`;

CREATE TABLE `r_group_role` (
  `group_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_group_role` */

insert  into `r_group_role`(`group_id`,`role_id`) values (10,41),(10,44),(10,46),(11,42),(12,45);

/*Table structure for table `r_group_user` */

DROP TABLE IF EXISTS `r_group_user`;

CREATE TABLE `r_group_user` (
  `group_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`,`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_group_user` */

insert  into `r_group_user`(`group_id`,`username`) values (10,'hz0001'),(11,'sh0001'),(11,'zhangsan'),(12,'hz0002');

/*Table structure for table `r_role` */

DROP TABLE IF EXISTS `r_role`;

CREATE TABLE `r_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `role_type` tinyint(4) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

/*Data for the table `r_role` */

insert  into `r_role`(`role_id`,`role_name`,`role_type`) values (6,'超级管理员',1),(41,'杭分财务',2),(42,'上分财务',2),(44,'杭分会计',2),(45,'杭分综合部经理',2),(46,'审计专员',2);

/*Table structure for table `r_role_permission` */

DROP TABLE IF EXISTS `r_role_permission`;

CREATE TABLE `r_role_permission` (
  `role_id` int(11) NOT NULL,
  `sf_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`sf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_role_permission` */

insert  into `r_role_permission`(`role_id`,`sf_id`) values (6,3),(6,8),(6,20),(6,64),(6,69),(6,130),(6,131),(6,132),(6,135),(6,136),(6,137),(6,145),(6,146),(6,147),(6,148),(6,149),(6,150),(6,151),(6,152),(6,153),(6,154),(6,155),(6,157),(6,158),(6,159),(6,160),(6,162),(6,163),(6,164),(6,165),(6,166),(6,167),(6,168),(6,169),(6,170),(6,171),(6,174),(6,175),(41,69),(42,69),(44,69),(45,69),(45,153),(45,154),(45,155);

/*Table structure for table `r_sys_function` */

DROP TABLE IF EXISTS `r_sys_function`;

CREATE TABLE `r_sys_function` (
  `sf_id` int(11) NOT NULL AUTO_INCREMENT,
  `sr_id` int(11) NOT NULL,
  `operate_code` varchar(20) NOT NULL,
  `operate_name` varchar(50) NOT NULL,
  `url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`sf_id`,`operate_code`,`sr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=utf8;

/*Data for the table `r_sys_function` */

insert  into `r_sys_function`(`sf_id`,`sr_id`,`operate_code`,`operate_name`,`url`) values (3,5,'view','查看','listRUser.do'),(8,8,'view','查看','listAllMenu.do'),(20,17,'monitor','后台监控',NULL),(64,6,'view','查看角色','listRRole.do'),(69,46,'view','查看','listOrderInfo.do'),(130,49,'view','查看','listAllGroup.do'),(131,5,'resetPassword','重置密码','resetUserPassword.do'),(132,5,'setUserRole','设置用户角色','setUserRoles.do'),(135,6,'add','新增角色','addRRole.do'),(136,6,'update','修改角色','updateRRole.do'),(137,6,'del','删除角色','delRRole.do'),(145,49,'optRoot','操作节点',NULL),(146,49,'save','保存用户组','addRGroup.do'),(147,49,'addGroupUser','添加成员','addGroupUser.do'),(148,49,'removeGroupUser','删除成员','delRGroupUser.do'),(149,49,'addRole','添加角色','addRGroupRole.do'),(150,49,'delRole','删除角色','delRGroupRole.do'),(151,49,'viewGroupUser','查看用户组成员','listRGroupUser.do'),(152,49,'viewGroupRole','查看用户组角色','listRoleByGroupId.do'),(153,46,'add','添加','addOrderInfo.do'),(154,46,'update','修改','updateOrderInfo.do'),(155,46,'del','删除','delOrderInfo.do'),(157,5,'addRUser','新增用户','addRUser.do'),(158,5,'addRole','添加角色','listUserRRole.do'),(159,6,'listRoleRelationInfo','角色关系','listRoleRelationInfo.do'),(160,6,'listRoleMenu','获取角色菜单','listRoleMenu.do'),(162,8,'save','添加资源','addRSysRes.do'),(163,8,'update','修改资源','updateRSysRes.do'),(164,8,'addOpt','添加操作权限','addSysFunction.do'),(165,8,'delOpt','删除操作权限','delRSysFunction.do'),(166,8,'listOpt','查询操作权限','listSysFunctionBySrId.do'),(167,8,'delRes','删除资源','delRSysRes.do'),(168,49,'updateGroup','修改用户组','updateRGroup.do'),(169,49,'delRGroup','删除用户组','delRGroup.do'),(170,49,'listGroupNoAddUser','查询用户组未分配用户','listGroupNoAddUser.do'),(171,8,'optRoot','操作节点',''),(174,8,'setSysFunctionRole','授权','setSysFunctionRole.do'),(175,6,'listRolePermission','获取角色权限','listRolePermissionByRoleId.do');

/*Table structure for table `r_sys_operate` */

DROP TABLE IF EXISTS `r_sys_operate`;

CREATE TABLE `r_sys_operate` (
  `operate_code` varchar(20) NOT NULL,
  `operate_name` varchar(50) NOT NULL,
  PRIMARY KEY (`operate_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_sys_operate` */

insert  into `r_sys_operate`(`operate_code`,`operate_name`) values ('cancelOrder','废除订单'),('checkOrder','查看订单'),('del','删除'),('delEmail','删除邮件'),('monitor','后台监控'),('qiaoqiaohua','悄悄话'),('sendEmail','发送邮件'),('update','修改'),('validate','验证'),('view','查看');

/*Table structure for table `r_sys_res` */

DROP TABLE IF EXISTS `r_sys_res`;

CREATE TABLE `r_sys_res` (
  `sr_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `res_name` varchar(50) NOT NULL,
  `url` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`sr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

/*Data for the table `r_sys_res` */

insert  into `r_sys_res`(`sr_id`,`parent_id`,`res_name`,`url`) values (4,0,'后台管理',''),(5,4,'用户管理','sys/user.jsp'),(6,4,'角色管理','sys/role.jsp'),(8,4,'资源管理','sys/sysRes.jsp'),(17,4,'监控页面','sys/monitor.jsp'),(18,0,'订单管理',''),(46,18,'订单查询','sys/order.jsp'),(49,4,'用户组管理','sys/group.jsp');

/*Table structure for table `r_user` */

DROP TABLE IF EXISTS `r_user`;

CREATE TABLE `r_user` (
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `add_time` datetime NOT NULL,
  `last_login_date` datetime DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_user` */

insert  into `r_user`(`username`,`password`,`add_time`,`last_login_date`) values ('admin','1000:990a6be8d9842700b8ce2f3ee6dd1594e222069a3fbc2d0a:25724de735fde25b38a9373b59f07e636214c5729e571a74','2014-07-21 15:01:18','2015-12-04 10:54:43'),('hz0001','1000:3bdff2e0d75ad091908032aa289188db14e92541eb214853:865bfee64557f237d3434c4a02bb0d3fecdc9892b798e08c','2015-10-28 10:46:52','2015-11-03 15:15:57'),('hz0002','1000:befb1d4438cc5eed5bdac48e61de0f522cd7aeff56763928:f5d104313d24651d9f80dd5dd2f95f0c8b0c3db454eeea75','2015-10-28 10:47:03','2015-11-03 15:15:38'),('sh0001','1000:4b101bfd2a6d04c68a51f1bb01cd6d1008f66572c3050a4f:4b163e5917b3214a156fb7ad9cd281fe568503be2f6a2c6e','2015-10-28 10:47:41','2015-11-03 15:15:27'),('zhangsan','1000:e8e98410ddec95bd142da98b26db9499c83c599ba96f6dca:b4399660ffd0f70b61040959785a3312217899685a192d13','2015-11-03 13:55:25','2015-11-03 13:58:20');

/*Table structure for table `r_user_role` */

DROP TABLE IF EXISTS `r_user_role`;

CREATE TABLE `r_user_role` (
  `username` varchar(50) NOT NULL,
  `role_id` int(11) NOT NULL,
  `role_type` tinyint(4) NOT NULL,
  PRIMARY KEY (`username`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `r_user_role` */

insert  into `r_user_role`(`username`,`role_id`,`role_type`) values ('admin',6,1),('hz0001',41,2),('hz0001',44,2),('hz0001',46,2),('hz0002',45,2),('sh0001',42,2),('zhangsan',42,2);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
